#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka/Contigs"    ;
my $mart = "/home/ana/LAB/Virus/Databases/VMR_20-190822_MSL37.2_DNA-viruses.tsv" ;
my $filt = "/home/ana/LAB/Virus/PhiX174/BLASTs/phiX174_MCPs2rm.tsv" ;
my $ecos = "Ecosystems.tab" ;

my (%Classes, %bigFams) ;
open (M, "$mart") || die "Can't open $mart\n" ;
<M> ;
while (<M>) {
	chomp ;
	my ($class, $genus) = (split/\t/,$_)[7,13] ;
	unless ($genus) {
		$genus = "NotAvailable" ;
	}
	$bigFams{$genus} = $class ;
	$Classes{$class} = 1 ;
#	print "*$genus* -> *$class*\n" ; <STDIN> ;
} close M ;

my %filtout ;
open (PHI, "$filt") || die "Can't open $filt\n" ;
while (<PHI>) {
	chomp ;
	my $gene = (split/\t/,$_)[0] ;
	$filtout{$gene} = 1 ;
} close PHI ;

my %ecos; 
open (ECOS, "$ecos") || die "Can't open $ecos\n" ;
while (<ECOS>) {
	chomp ;
	next if ($_ =~ /^#/) ;
	my ($mg, $ecosys) = (split/\t/,$_)[0,1] ;
	$ecos{$mg} = $ecosys ;
} close ECOS ;

my %labels = ( "Freshwater plankton" => '"#7EA310"',
	"Marine plankton" => '"#429EBD"',
	"Solar Saltern plankton" => '"#FF5F81"',
	"Marine sediments" => '"#053F5C"',
	"Lake sediments" => '"#2D6509"',
	"Soil" => '"#6C341C"',
	"Microbial Mat" => '"#6638B7"',
	"Microbialite" => '"#F27F0C"',
	"Host-associated microbiomes" => '"#C1000F"') ;
	           
my (%MCPtx, %vrFam, %Balti) ;
my $newTx = "All_vpf-class_Collapsed50.tsv" ;
open (NT, "$path/$newTx") || die "Can't open $path/$newTx\n" ;
<NT> ;
while (<NT>) {
	chomp ;
	my ($contig, $MCPtx, $Baltimore, $taxa) = (split/\t/,$_)[0,3,4,6] ;
#	print "$contig, $MCPtx, $Baltimore, $taxa" ; <STDIN> ;
	if ($taxa eq "NA") {
		$taxa = "NotAssigned" ;
	}
	if ($Baltimore eq "NA") {
		$Baltimore = "NotAssigned" ;
	}
	$MCPtx{$contig} = $MCPtx     ;
	$vrFam{$contig} = $taxa      ;
	$Balti{$contig} = $Baltimore ;
} close NT ;

my $virout = "ALL_50perc_ViralMCPs_NewTx_RPKMs.melted" ;
my $phgout = "ALL_50perc_PhageMCPs_NewTx_RPKMs.melted" ;

open (VOUT,  ">$path/$virout") || die "Can't create $path/$virout\n" ;
open (POUT,  ">$path/$phgout") || die "Can't create $path/$phgout\n" ;

foreach my $rpkmsF (<$path/Tables/*_50perc_*MCPs_RPKMs.melted>) {
	my $fn = (split/\//,$rpkmsF)[-1] ;
	my $mg = (split/\_50per/,$fn)[0] ;
	next if ($mg eq "ALL") ;
	next if ($mg eq "YTHB") ;
	next if ($mg eq "HOM-BMfemale4") ;
	next if ($mg eq "HOM-BMmale3") ;
	next if ($mg =~ /SAIE/) ;
	next if ($mg =~ /WC-Sal32/) ;
	print "$mg\n" ; #<STDIN> ;
	
	open (RPKMS, "$rpkmsF") || die "Can't open $rpkmsF\n" ;
	
#	<RPKMS> ;
	while (<RPKMS>) {
		chomp ;
		my ($contig, $geneID, $rpkms) = (split/\t/,$_)[0,1,6] ;
		next if ($filtout{$geneID}) ;
		my $color ;
		if ($vrFam{$contig} eq "NotAssigned") {
			$color = $vrFam{$contig} ;
		} elsif ($bigFams{$vrFam{$contig}}) {
			$color = $bigFams{$vrFam{$contig}} ;
		} elsif ($vrFam{$contig} eq "Bxz1virus" || $vrFam{$contig} eq "N4virus" || $vrFam{$contig} eq "T4virus" || $vrFam{$contig} eq "Che8virus" || $vrFam{$contig} eq "Vp5virus" || $vrFam{$contig} eq "Kp34virus" || $vrFam{$contig} eq "E125virus" || $vrFam{$contig} eq "Luz24virus" || $vrFam{$contig} eq "P2virus" || $vrFam{$contig} eq "Msw3virus"  || $vrFam{$contig} eq "Che9cvirus" || $vrFam{$contig} eq "P22virus" || $vrFam{$contig} eq "Pis4avirus" || $vrFam{$contig} eq "Ff47virus" || $vrFam{$contig} eq "Bpp1virus" || $vrFam{$contig} eq "P12024virus" || $vrFam{$contig} eq "Nit1virus" || $vrFam{$contig} eq "Cba41virus" || $vrFam{$contig} eq "Bcep22virus" || $vrFam{$contig} eq "P70virus" || $vrFam{$contig} eq "Cp220virus" || $vrFam{$contig} eq "Cba181virus" || $vrFam{$contig} eq "T5virus" || $vrFam{$contig} eq "Phicbkvirus" || $vrFam{$contig} eq "Bc431virus" || $vrFam{$contig} eq "Pa6virus" || $vrFam{$contig} eq "Sfi11virus" || $vrFam{$contig} eq "L5virus" || $vrFam{$contig} eq "P23virus" || $vrFam{$contig} eq "Xp10virus" || $vrFam{$contig} eq "Cr3virus" || $vrFam{$contig} eq "B4virus" || $vrFam{$contig} eq "P12002virus" || $vrFam{$contig} eq "D3112virus" || $vrFam{$contig} eq "Sk1virus" || $vrFam{$contig} eq "Spo1virus" || $vrFam{$contig} eq "C5virus" || $vrFam{$contig} eq "Cd119virus" || $vrFam{$contig} eq "Cp51virus" || $vrFam{$contig} eq "Epsilon15virus" || $vrFam{$contig} eq "Hp1virus" || $vrFam{$contig} eq "Kf1virus" || $vrFam{$contig} eq "Kp36virus" || $vrFam{$contig} eq "M12virus" || $vrFam{$contig} eq "Pepy6virus" || $vrFam{$contig} eq "Phi29virus" || $vrFam{$contig} eq "Phieco32virus" || $vrFam{$contig} eq "Sap6virus" || $vrFam{$contig} eq "Sp6virus" || $vrFam{$contig} eq "Tm4virus" || $vrFam{$contig} eq "Tp21virus" || $vrFam{$contig} eq "Prtbvirus" || $vrFam{$contig} eq "P1virus" || $vrFam{$contig} eq "Vi1virus" || $vrFam{$contig} eq "Cp8virus" || $vrFam{$contig} eq "Sitaravirus" || $vrFam{$contig} eq "P68virus" || $vrFam{$contig} eq "Phic31virus" || $vrFam{$contig} eq "Phijl1virus" || $vrFam{$contig} eq "Rslunavirus" || $vrFam{$contig} eq "Schizot4virus" || $vrFam{$contig} eq "Se1virus" || $vrFam{$contig} eq "Secunda5virus" || $vrFam{$contig} eq "Septima3virus" || $vrFam{$contig} eq "Una961virus" || $vrFam{$contig} eq "V5virus" || $vrFam{$contig} eq "C2virus" || $vrFam{$contig} eq "Sfi21dt1virus") {
			$color = "Caudoviricetes" ;
		} elsif ($vrFam{$contig} eq "Mimiviridae" || $vrFam{$contig} eq "Phycodnaviridae" || $vrFam{$contig} eq "Catovirus") {
			$color = "Megaviricetes" ;
		} elsif ($vrFam{$contig} eq "Microviridae") {
			$color = "Malgrandaviricetes" ;
		} elsif ($vrFam{$contig} eq "Lavidaviridae") {
			$color = "Maveriviricetes" ;
		} elsif ($vrFam{$contig} eq "Adenoviridae" || $vrFam{$contig} eq "Tectivirus") {
			$color = "Tectiliviricetes" ;
		} elsif ($vrFam{$contig} eq "Pleolipoviridae") {
			$color = "Huolimaviricetes" ;
		} elsif ($vrFam{$contig} eq "Poxviridae") {
			$color = "Pokkesviricetes" ;
		} elsif ($vrFam{$contig} eq "Globulovirus") {
			$color = "Globuloviridae" ;
		} elsif ($vrFam{$contig} eq "Pandoravirus") {
			$color = "Pandoraviridae" ;
		} elsif ($vrFam{$contig} eq "Ambidensovirus") {
			$color = "Quintoviricetes" ;
		} elsif ($vrFam{$contig} eq "Gammasphaerolipovirus") {
			$color = "Laserviricetes" ;
		} elsif ($vrFam{$contig} eq "Ichnovirus") {
			$color = "Naldaviricetes" ;
		} elsif ($vrFam{$contig} eq "Rudivirus") {
			$color = "Tokiviricetes" ;
		} elsif ($vrFam{$contig} eq "Bicaudavirus") {
			$color = "Bicaudaviridae" ;
		} else {
			if ($Classes{$vrFam{$contig}}) {
				$color = $vrFam{$contig} ;
			} elsif ($vrFam{$contig} =~ /virus/) {
				print "$contig: **$vrFam{$contig}**\t" ; #<STDIN> ;
				my $suf = (split/virus/,$vrFam{$contig})[0] ;
				if ($bigFams{$suf}) {
					$color = $bigFams{$suf} ;
					print "(NL) $vrFam{$contig} matches family $bigFams{$suf} -> $color\n" ;# <STDIN> ;
				} else {
					$suf =~ tr/a-z/A-Z/ ;
					if ($bigFams{$suf}) {
						$color = $bigFams{$suf} ;
						print "(CL) $vrFam{$contig} matches family $bigFams{$suf} -> $color\n" ;# <STDIN> ;
					} else {
						$suf = (split/virus/,$vrFam{$contig})[0] ;
						if ($Classes{$suf}) {
							$color = $Classes{$suf} ;
							print "(NL) $vrFam{$contig} matches class $Classes{$suf} -> $color\n" ;# <STDIN> ;
						} else {
							$suf =~ tr/a-z/A-Z/ ;
							if ($Classes{$suf}) {
								$color = $Classes{$suf} ;
								print "(CL) $vrFam{$contig} matches class $Classes{$suf} -> $color\n" ;# <STDIN> ;
							} else {
								$color = "Other" ;
								print "(1) $vrFam{$contig} no match in Mart's DB\n" ; #<STDIN> ;
							}
						}
					}
				}
			} else {
				$color = "Other" ;
				print "(2) $vrFam{$contig} no match in Mart's DB\n" ; #<STDIN> ;
			}
		}
		if ($MCPtx{$contig} eq "Prophage") {
			print      "$contig\t$geneID\t$mg\t$MCPtx{$contig}\t$vrFam{$contig}\t$Balti{$contig}\t$color\t$rpkms\t$ecos{$mg}\t$labels{$ecos{$mg}}\n" ; #<STDIN> ;
			print POUT "$contig\t$geneID\t$mg\t$MCPtx{$contig}\t$vrFam{$contig}\t$Balti{$contig}\t$color\t$rpkms\t$ecos{$mg}\t$labels{$ecos{$mg}}\n" ;
		} elsif ($MCPtx{$contig} eq "Virus") {
			print      "$contig\t$geneID\t$mg\t$MCPtx{$contig}\t$vrFam{$contig}\t$Balti{$contig}\t$color\t$rpkms\t$ecos{$mg}\t$labels{$ecos{$mg}}\n" ; #<STDIN> ;
			print VOUT "$contig\t$geneID\t$mg\t$MCPtx{$contig}\t$vrFam{$contig}\t$Balti{$contig}\t$color\t$rpkms\t$ecos{$mg}\t$labels{$ecos{$mg}}\n" ; 
		} else {
			print "WARNING: $contig - $geneID have no MCP Taxa assigned" ; <STDIN> ;
		}
	} close RPKMS ;
}
