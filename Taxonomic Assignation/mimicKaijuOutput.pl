#!/usr/bin/perl -w

use strict ;

my $diamond = $ARGV[0] ;
my $covT    = $ARGV[1] ;
my $idnT    = $ARGV[2] ;


unless ($diamond) {
	print "Diamond output: " ;
	chomp ($diamond = <STDIN>) ;
}

unless ($covT) {
	print "Coverage threshold was not specified. Default 70% will be used\n" ;
	$covT = 70 ;
}

unless ($idnT) {
	print "Identity threshold was not specified. Default 35% will be used\n" ;
	$idnT = 35 ;
}


my ($name, $out) ;

if ($diamond =~ /^WC-Sal/) {
	my ($met, $filt) = (split/\_/,$diamond)[0,1] ;
	$name = "$met"."_"."$filt" ;
} else {
	$name = (split/\_/,$diamond)[0] ; #print $name ;
}
$out  = "$name"."_"."$covT"."c_"."$idnT"."i_BestHit.kjlk" ;

my %BH ;


open (DMD, "$diamond") || die "Can't open $diamond\n" ;
open (OUT, ">$out")    || die "Can't create $out\n"   ;

while (<DMD>) {
	chomp ;
	my $line = $_ ;
	next if ($line =~ /^diamond/) ;
#	if ($line =~ /$name/) {
		my ($query, $sseqid, $ident, $length, $qstart, $qend, $bit) = (split/\t/,$line)[0, 1, 2, 7, 8, 9, 11] ;
		next if ($ident <= $idnT) ;
#		print "$query, $sseqid, $ident, $length, $qstart, $qend, $bit" ; <STDIN> ;
		next if ($sseqid eq "*") ;
		my $AccN = "" ;
		if ($sseqid =~ /^gi\|/) {
			$AccN = (split/\|/,$sseqid)[3] ;
		} else {
			$AccN = $sseqid ;
		}
#		my $taxID = "" ;
		my $qCov  = (($qend - $qstart)/$length)*100 ; 		### For Blastp
#		my $qCov  = ((($qend - $qstart)/3)*100)/$length ;  	### For blastx
		next if ($qCov <= $covT) ;
#		print "$query\t$AccN\t$qCov\t$ident\t$bit\n" ; <STDIN> ;
		if ($BH{$query}) { # << $bit) {
#			print "discard $AccN\t***->$BH{$query} vs $bit<-***\t$query\t$AccN\t$qCov\t$ident\t$bit\n" ;
			next ;
		} else {
			$BH{$query} = $bit ;
#			print "keep $AccN\t->$BH{$query} vs $bit<-\n**-$query-\t$AccN\t$qCov\t$ident\t$bit**\n" ; <STDIN> ;
			print "$query\t$AccN\t$qCov\t$ident\t$bit\n" ; #<STDIN> ;
			
			my @pieces = (split/\_/,$AccN) ;
			my $taxID  = pop @pieces ;
			my $gi ;
			foreach my $piece (@pieces) {
				$gi .= "$piece"."_" ;
			}
			chop $gi ;
			
			print OUT "C\t$query\t$taxID\t$bit\t$taxID,\t$gi,\tNA\n" ; #<STDIN> ;
			
		#1	either C or U, indicating whether the read is classified or unclassified.
		#2	name of the read
		#3	NCBI taxon identifier of the assigned taxon
		#4	the length or score of the best match used for classification
		#5	the taxon identifiers of all database sequences with the best match
		#6	the accession numbers of all database sequences with the best match
		#7	matching fragment sequence(s)
			
		}
#	} else {
#		next ;
#	}
}




