#!/usr/bin/perl -w

use strict ;

my $pfam = $ARGV[0] ;
my $taxa = $ARGV[1] ;


unless ($pfam) {
	print "PFAM column output (e.g. WC-Sal32_02v_Pfam-A.hmmblout): " ;
	chomp ($pfam = <STDIN>) ;
}

unless ($taxa) {
	print "Taxa tab output (e.g. WC-Sal32_02v_TaxaGroup.tab): " ;
	chomp ($pfam = <STDIN>) ;
}

my ($name, $out) ;

if ($pfam =~ /^WC-Sal/) {
	my ($met, $filt) = (split/\_/,$pfam)[0,1] ;
	$name = "$met"."_"."$filt" ;
} else {
	$name = (split/\_/,$pfam)[0] ; #print $name ;
}
$out  = "$name"."_PFAM_Taxa.tab" ;

open (TX,  "$taxa") || die "Can't open $taxa\n"  ;
open (PF,  "$pfam") || die "Can't open $pfam\n"  ;
open (OUT, ">$out") || die "Can't create $out\n" ; 

my %taxa ;
while (<TX>) {
	chomp ;
	my ($id, $cl) = (split/\t/,$_)[0,2] ;
	$taxa{$id} = $cl ;
}
print "Taxa hashed\n" ;
close TX ;

while (<PF>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($id, $name, $pf) = (split,$_)[0,2,3] ;
	my $taxa ;
	if ($taxa{$id}) {
		$taxa = $taxa{$id} ;
	} else {
		$taxa = "NA" ;
	}
	print "$id\t$name\t$pf\t$taxa\n" ; #<STDIN> ;
	print OUT "$id\t$name\t$pf\t$taxa\n" ;
}

