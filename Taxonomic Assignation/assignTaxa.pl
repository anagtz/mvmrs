#!/usr/bin/perl -w

use strict ;

#my $path  = "/home/anagtz/LAB/Virus/DNApolB" ;
my $class = "CPR_DPANN_others_stats.tab"     ;
#my $out   = "WC-readsTaxGroup.tab"	 ;

my (%spp, %reads, %metas) ;

my $kaiju = $ARGV[0] ;

unless ($kaiju) {
	print "Kaiju files with lineages (e.g. WC-EdV02v_R1_paired_Kaiju_BestHit.names): " ;
	chomp ($kaiju = <STDIN>) ;
}

open (CPR, $class) || die "$class file is not in wd\n" ;
close CPR ;

my ($name, $out) ;

if ($kaiju =~ /^WC-Sal/) {
	my ($met, $filt) = (split/\_/,$kaiju)[0,1] ;
	$name = "$met"."_"."$filt" ;
} else {
	$name = (split/\_/,$kaiju)[0] ; print "$name\n" ; # <STDIN> ;
}
$out  = "$name"."_"."TaxaGroup.tab" ;


open (OUT, ">$out") || die "Can't create $out\n" ;

my $fix = "$name"."_fixTheseBugs.txt" ;
open (FIX, ">$fix") || die "Can't create warnings file $fix\n" ;


#foreach my $kaiju (<WC-*_BestHit.names>) {
#	my $fn   = (split/\//,$kaiju)[-1] ;
#	my $meta = (split/\_/,$kaiju)[0]     ;
	
#	my $reads = "$meta"."_readsTaxGroup.tab" ;
	
open (KJ, "$kaiju") || die "Can't open $kaiju\n" ;
while (<KJ>) {
	chomp ;
	my ($id, $lin) = (split/\t/,$_)[1,7] ;
	next unless $lin ;
	my $sp  = (split/\;/,$lin)[6] ;
	if ($sp =~ /^\s/) {
		$sp =~ s/\s// ;
	}
	$sp =~ s/\[// ;
	$sp =~ s/\]// ;
	my $nv = "" ;
	my $kingdom = (split/\;/,$lin)[0] ;
	if ($kingdom eq "Eukaryota") {
		$nv = "Eukaryota" ;
	} elsif ($kingdom eq "Viruses") {
		$nv = "Virus" ;
	} else {
		if ($sp eq "NA") {
#			print "$sp ... -> " ;
			my $cinco = (split/\;/,$lin)[5] ;
			$cinco =~ s/\s// ;
			if ($cinco eq "NA") {
				my $cuatro = (split/\;/,$lin)[4] ;
				$cuatro =~ s/\s// ;
				if ($cuatro eq "NA") {
					my $tres = (split/\;/,$lin)[3] ;
					$tres =~ s/\s// ;
					if ($tres eq "NA") {
						my $dos = (split/\;/,$lin)[2] ;
						$dos =~ s/\s// ;
						if ($dos eq "NA") {
							my $uno = (split/\;/,$lin)[1] ;
							$uno =~ s/\s// ;
							if ($uno eq "NA") {
	#							print "is $lin only composed of NAs??\n" ;# <STDIN> ;
								$sp = (split/\;/,$lin)[0] ;
								if ($sp eq "Archaea") {
									$nv = "Other Archaea" ;
								} elsif ($sp eq "Bacteria") {
									$nv = "Other Bacteria" ;
								} else {
									print FIX "$lin?????" ; # <STDIN> ;
								}
							} else {
								$sp = $uno ;
							}
						} else {
							$sp = $dos ;
						}
					} else {
						$sp = $tres ;
					}
				} else {
					$sp = $cuatro ;
				}
			} else {
				$sp = $cinco ;
			}
#			print "now $sp ($lin)" ; #<STDIN> ;
		}
	
		unless ($nv) {
			if ($sp eq "Halobacteria") {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /haloarchaeon/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /DPANN/) {
				$nv = "DPANN" ;
			} elsif ($sp =~ /verrucomicrobium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /proteobacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /esnapd21/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Halosiccatus/) {
            	$nv = "Other Archaea" ;
            } elsif ($sp =~ /Lentisphaerae/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Caldicellulosiruptor/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Ilumatobacteraceae/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Laribacter/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Desulfoconvexum/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /sulfate-reducing/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /sulfur-oxidizing/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Myxococcus/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Intrasporangium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Sphaerobacter/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Criblamydiaceae/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Desulfosalsimonas/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Thermosediminibacter/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Trichodesmium/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Desulfatibacillum/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Moeniiplasma/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Streptacidiphilus/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Oligotropha/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Acidobacteria/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Acrocarpospora/) {
                                $nv = "Other Bacteria" ;
                        } elsif ($sp =~ /Nostocoida/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Syngnamydia/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thermomonosporaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Leptotrichiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /A1Q1_fos/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Westiella/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Chromobacteriaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /BAC13K9BAC/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /nuHF2/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Odoribacteraceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Hafniaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Dermacoccaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Methylobacteriaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Peptoniphilaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Nanopelagicaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured bacterium \w+/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured bacterium \d+/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured bacterium \(/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured \w+ bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured \w+ \w+ bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desulfurispirillum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured \w+ \w+ \w+ bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured \w+\-\w+ bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /bacterial symbiont/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /symbiont/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /degrading bacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /cyanobacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pegethrix/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Barnesiellaceae/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Rhabdochlamydia/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Dermabacteraceae/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Ktedonobacteria/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Polymorphospora/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Streptococcaceae/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pyrinomonas/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Starkeya/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Teredinibacter/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Saccharophagus/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Acidithiomicrobium/) {
                $nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desmonostoc/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Aetherobacter/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Aggregicoccus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Cystobacterineae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Annwoodia/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Archangiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Cardiobacteriaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Nautiliaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Chloroflexi/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /cosmid clone/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Cronbergia/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Heteroscytonema/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Solibacteraceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Buchnera/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Orbaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Gloeobacter/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Rivulariaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Roholtiella/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Stigonema/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thermaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thermoleptolyngbya/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Tolypothrichaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /uncultured bacteria/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Australian soil clone/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Morganellaceae/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Nocardiaceae/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Desulfonatronobacter/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Nardonella/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Steffania/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Pectobacteriaceae/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Nitrosococcus/) {
            	$nv = "Other Bacteria" ;
            } elsif ($sp =~ /Thiococcus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Microseira/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pseudenhygromyxa/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Alvinella/) {
				$nv = "Eukaryota" ;
			} elsif ($sp =~ /Propionigenium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pyxidicoccus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pullulanibacillus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thermochromatium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Anaerocellum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Aphanothecaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Chlamydiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /phytoplasma/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Racemicystis/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /planctomycete/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Kofleria/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /magneto-ovoid/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Megaira/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Chlorobiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Eleftheria/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Luteolibacter/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Aphanizomenonaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /actinobacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /myxobacterium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Synechococc/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Leptolyngbyaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Methylosulfonomonas/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Pleurocapsales/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Microchaete/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Methylacidimicrobium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Heliobacillus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thiolamprovum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Simulacricoccus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Actinoallomurus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Magnetananas/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Chroococcaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Gloeothece/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Carbophilus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Beutenbergiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Epixenosoma/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Halochromatium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Jahnella/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Lamprobacter/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Oscillatoriaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Oscillochloridaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Symplocastrum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Deinococcus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Phaselicystis/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Oculatella/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desulfohalobiaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desulforhabdus/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desulfurispirillum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Natranaerobius/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Rhodobaculum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Thermobaculum/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Rhabdochromatium/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /Desulfurobacteriaceae/) {
				$nv = "Other Bacteria" ;
			} elsif ($sp =~ /thaumarchaeote/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /euryarchaeote/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /uncultured archaeon Med/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /crenarchaeote/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Thalassoarchaea/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /uncultured archaeon/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Methanobacteria/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Methanococcus/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Natribaculum/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Natronobiforma/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Thermococcaceae/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Sulfolobaceae/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /korarchaeote/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Salinirubrum/) {
				$nv = "Other Archaea" ;
			} elsif ($sp =~ /Acetothermum/) {
				$nv = "CPR" ;
			} elsif ($sp =~ /Gracilibacteria/) {
                                $nv = "CPR" ;
                        } elsif ($sp =~ /Vermiphilus/) {
				$nv = "CPR" ;
			} elsif ($sp ne "NA") {
				my $match = `grep \"$sp\\b\" $class` ;
				if ($match) {
					chomp $match ;
					if ($match =~ /n/) {
						my $first = (split/\n/,$match)[0] ;
						$match    = $first ;
					}
					$nv = (split/\t/,$match)[0] ;
#					print "$match ... [OK] -> $nv\n" ; #<STDIN> ;
				} else {
					if ($sp =~ /phage/ || $sp =~ /virus/) {
						$nv = "Virus" ;
#						print "$sp ... [OK] -> $nv\n" ;# <STDIN> ;
					} elsif ($sp =~ /Candidatus/) {
						my $search = (split/ /,$sp)[1] ;
						my $nmatch = `grep \"$search\\b\" $class` ;
						if ($nmatch) {
							chomp $nmatch ;
							$nv = (split/\t/,$nmatch)[0] ;
#							print "$nmatch ... [OK] -> $nv\n" ;# <STDIN> ;
						} else {
#							print "2. $search no match in $path/$class (LIN: $spp{$sp})\n" ; <STDIN> ;
						}
					} elsif  ($sp =~ /uncultured marine group II/) {
						my $search = (split/\//,$sp)[0] ;
						my $nmatch ;
						if ($search =~ /\./) {
							$search =~ s/\./\\\./g ;
							$nmatch = `grep -r \"$search\" $class` ;
						} else {
							$nmatch = `grep -e \"$search\\b\" $class` ;
						}
				#		my $nmatch = `grep -e \"$search\\b\" $class` ;
						if ($nmatch) {
							chomp $nmatch ;
							$nv = (split/\t/,$nmatch)[0] ;
#							print "$nmatch ... [OK] -> $nv\n" ;# <STDIN> ;
						} else {
			#				print "3. $search no match in $path/$class (LIN: $spp{$sp})\n" ; <STDIN> ;
						}
					} else {
						my ($search, $two, $three) = (split/ /,$sp)[0,1,2] ;
						if ($search eq "uncultured") {
							$search = "$two"." "."$three" ;
						}
						my $nmatch ;
						if ($search =~ /\./) {
							$search =~ s/\./\\\./g ;
							$nmatch = `grep -r \"$search\" $class` ;
						} else {
							$nmatch = `grep -e \"$search\\b\" $class` ;
						}
						if ($nmatch) {
							chomp $nmatch ;
							$nv = (split/\t/,$nmatch)[0] ;
#							print "$nmatch ... [OK] -> $nv\n" ; #<STDIN> ;
						} else {
#							print "4. $search no match in $path/$class (LIN: $spp{$sp})\n" ; <STDIN> ;
						}
					}
					unless ($nv) {
						print FIX "1. $sp no match in $class (LIN: $spp{$sp})\n" ; #<STDIN> ;
						$nv = "NA" ;
					}
				} 
			} else {
					$nv = "NA" ;
					print FIX "$id\t$lin\n" ; # <STDIN> ;
			}
		}
	}
#	print "$read\t$sp\t$nv\n" ; <STDIN> ;
#	if ($nv =~ /CATEGORY/) {
#		print "$read\t$lin\n" ; <STDIN> ;
#	}
#	if ($id eq "WC-MedS02v_00447") {
#		print "$id\t$sp\t$nv\n" ; <STDIN> ;
#	}
	print OUT "$id\t$sp\t$nv\n" ;
}
