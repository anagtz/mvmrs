#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka/Diversity" ;

	foreach my $clstr (<$path/USCGs/Clusters/*.clstr>) {
#		print "FILE: $clstr\n" ;
		my $fn  = (split/\//,$clstr)[-1] ; #print "$fn!\n" ; <STDIN> ;
		my @fds = (split/\_/,$fn) ;
		my $tx  = pop @fds ;
		$tx =~ s/\.clstr// ;
		if ($tx eq "FL") {
			$tx  = pop @fds ;
		#	print "$tx!\n" ;
		}
		my $meta ;
		foreach my $field (@fds) {
			$meta .= "$field"."_" ;
		} chop $meta ;
		
#		print "$meta ($tx)\n" ; <STDIN> ;
		open (CL, "$clstr") || die "Can't open $clstr\n" ;
		my (@clusters, %samples) ;
		my %clusters ;
		my (%abund) ;
		$/ = "\>Cluster" ;
		<CL> ;
		my $pfam ;
		while (<CL>) {
			chomp ;
			my @lines = (split/\n/,$_) ;
			my $i     = shift @lines ;
			$i        =~ s/ //g ;
			my $id    = "$meta"."_"."$i" ;
			#print "$id\n" ;  <STDIN> ;
			push (@clusters, $id) ;
			
			foreach my $line (@lines) {
				my $header = (split/\>/,$line)[1]   ;
				my ($geneid, $pfam) = (split/\|/,$header)[0,1] ;
				my $sample = (split/\_\d+\d\b/,$geneid)[0] ;
#				print "$sample $id ($geneid)\n" ; <STDIN> ;
				$abund{$sample}{$id} ++ ;
				$samples{$sample} = 1 ;
				$clusters{$id} .= "$geneid," ;
			}
		}
		$/ = "\n" ;
		
		
#		my $out = "$meta"."_USCGs_"."$tx"."_AbundClusters.tab" ;
		my $out = "$meta"."_USCGs_"."$tx"."_Clusters_GeneIDs.tab" ;
		open (OUT, ">$out") || die "Can't open $out\n" ;
#		my $header = "Cluster\t" ;
#		foreach my $meta (sort keys %samples) {
#			$header .= "$meta\t" ;
#		}
#		chop $header ;
#		print OUT "$header\n" ;
		
		foreach my $cluster (sort @clusters) {
			my $line = "$cluster\t" ;
			foreach my $sample (sort keys %abund) {
				if ($abund{$sample}{$cluster}) {
					print "$cluster - $sample -> $abund{$sample}{$cluster}\n" ;# <STDIN> ;
					$line .= "$abund{$sample}{$cluster}\t" ;
				} else {
					print "$cluster - $sample -> 0\n" ; #<STDIN> ;
					$line .= "0\t" ;
				}
			}
			chop $line ;
#			print OUT "$line\n" ; #<STDIN> ;
			chop $clusters{$cluster} ;
			my @genes = (split/\,/,$clusters{$cluster}) ;
			my $count = scalar @genes ;
			print OUT "$cluster\t$count\t$clusters{$cluster}\n" ;
			print     "$cluster: $count: $clusters{$cluster}\n" ; #<STDIN> ;
		}
	}
