#!/usr/bin/perl -w

## From DivIndices.R: write.csv(divindx,"All_DivIndices_Prokka.csv")
## and divindec <- merge(divindx, ecosystems)
## write.csv(divindec,"All_DivIndices_Prokka_Ecosystems.csv")

use strict ;

## my (%viral, %cell)  ; ## Abundance in RPKMs terms
my (%vir, %phg, %cell) ; ## Abundance in RPKMs terms

my $RPKMs = "All_RPKMs_USCGsLarger_correctedMCPsSum_rn.melted" ;
open(RPKM, "$RPKMs") || die "Can't open $RPKMs\n"    ;
<RPKM> ;
while (<RPKM>) {
	chomp ;
	my ($type, $mg, $tx, $rpkms) = (split/\t/,$_)[0,2,3,4] ;
#	if ($rpkms eq "NA") {
#		print "$type\t$mg\t$rpkms" ; <STDIN> ;
#	}
	if ($type eq "Ribosomal") {
		$cell{$mg}    += $rpkms ;
	} else {
		if ($tx eq "Prophage") {
			$phg{$mg} += $rpkms ;
		} else {
			$vir{$mg} += $rpkms ;
		}
	}
} close RPKM ;


my $out = "All_Prokka_DivIndx_Ecosystems_RPKMs.tab"  ;
open (OUT, ">$out")  || die "Can't create $out\n"    ;

#my $divindx = "All_DivIndices_Prokka_Ecosystems.csv" ;
my $divindx = "All_DivIndices_VPsPhgs_Prokka_Ecosystems.csv" ;
open(DI, "$divindx") || die "Can't open $divindx\n"  ;

print OUT "Metagenome\tCellsAlphaDiv\tCellsEvenness\tCellsRPKMs\tViralAlphaDiv\tViralEvenness\tViralRPKMs\tPhageAlphaDiv\tPhageEvenness\tPhageRPKMs\tEcosystem\n" ;

<DI> ;
while (<DI>) {
	chomp ;
	
#	my ($mg, $aCel, $eCel, $aVir, $eVir, $ecos) = (split/\,/,$_)[1,2,5,6,9,10] ;
	my ($mg, $aCel, $eCel, $aVir, $eVir, $aPhg, $ePhg, $ecos) = (split/\,/,$_)[1,2,5,6,9,10,13,14] ;
	
	if ($aCel =~ /NA/) {
		print "Alpha Cells: $aCel" ; <STDIN> ;
	}
	if ($eCel =~ /NA/) {
		print "Evenness Cells: $eCel" ; #<STDIN> ;
	}
	if ($aVir =~ /NA/) {
		print "Alpha Virus: $aVir" ; <STDIN> ;
	}
	if ($eVir =~ /NA/) {
		print "Evenness Virus: $eVir" ; #<STDIN> ;
	}
	
	$mg   =~ s/\"//g ;
	$ecos =~ s/\"//g ;
	
	unless ($cell{$mg}) {
		$cell{$mg} = 0 ;
	}
	unless ($vir{$mg}) {
		$vir{$mg}  = 0 ;
	}
	unless ($phg{$mg}) {
		$phg{$mg}  = 0 ;
	}
	
#	print OUT "$mg\t$aCel\t$eCel\t$cell{$mg}\t$aVir\t$eVir\t$vir{$mg}\t$ecos\n" ;
	print OUT "$mg\t$aCel\t$eCel\t$cell{$mg}\t$aVir\t$eVir\t$vir{$mg}\t$aPhg\t$ePhg\t$phg{$mg}\t$ecos\n" ;
#	print     "$mg\t$aCel\t$eCel\t$cell{$mg}\t$aVir\t$eVir\t$vir{$mg}\t$ecos\n" ;
	print     "$mg\t$aCel*\t$eCel*\t$cell{$mg}**\t$aVir*\t$eVir*\t$vir{$mg}**\t$aPhg*\t$ePhg*\t$phg{$mg}**\t$ecos\n" ; #<STDIN> ;
} close DI ;

