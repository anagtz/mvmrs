#!/usr/bin/perl -w

use strict ;


#my $code = $ARGV[0] ;
my $desc = "USCGsMarkers.desc" ; #$ARGV[1] ;

#unless ($code) {
#	print "Metagenome code (e.g. DAL, LLA9, WC, etc...): " ;
#	chomp ($code = <STDIN>) ;
#}

#unless ($desc) {
#	print "File with genes (PFAM	desc)\nThe name of the file must match the name of the fasta files\n e.g. USCGsxn.desc and WC-*_USCGsxn.fna : " ;
#	chomp ($desc = <STDIN>) ;
#}

open (DESC, "$desc") || die "Can't open $desc\n" ;
my $genes = (split/\./,$desc)[0] ;

my %names ;

while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $name) = (split/\t/,$_) ;
	$names{$pfam} = $name ;
}

my @mthds = qw(Prokka OrfM) ;
foreach my $mth (@mthds) {
	my (%tot, %len, %min, %max, %txpf) ;
	my $suf = "$mth"."_USCGsMarkers_TaxaGroup.tab" ;
	foreach my $txgp (<Taxa/V*M*_$suf>) {
		my (%TX) ;
		my $fn = (split/\//,$txgp)[-1] ;
		my $mg = (split/\_/,$fn)[0]   ;
		open (TX, "$txgp") || die "Can't open $txgp\n" ;
		while (<TX>) {
			chomp ;
			my ($gene, $group) = (split/\t/,$_)[0,2]    ;
			my ($id, $pfam)    = (split/\|/,$gene)[0,1] ;
			next unless ($names{$pfam}) ;
			$TX{$id} = $group ;
		} close TX ;
		my $fna = "USCGs/"."$mg"."_"."$mth"."_"."$genes".".fna" ;
		open (FNA, "$fna") || die "Can't open $fna\n" ;
		print "$mg\n" ; # <STDIN> ;
		$/ = "\n\>" ;
#		<FNA> ;
		while (<FNA>) {
			chomp ;
			my @lines = (split/\n/,$_) ;
			my $head  = shift @lines ;
			$head =~ s/\>//g ;
			
			my ($id, $pfam) = (split/\|/,$head)[0,1]  ;
			next unless ($names{$pfam}) ;
		
			my $seq ;
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			
			my $l = length($seq) ;
			my ($class, $cpfam)  ;
			
			if ($TX{$id}) {
				$class = $TX{$id} ;
			} else {
				$class = "NA" ;
			}
			
			if ($class =~ /Archaea/) {
				$cpfam = "a_"."$pfam" ;
			} elsif ($class =~ /Bacteria/) {
				$cpfam = "b_"."$pfam" ;
			} elsif ($class =~ /CPR/) {
				$cpfam = "c_"."$pfam" ;
			} elsif ($class =~ /DPANN/) {
				$cpfam = "d_"."$pfam" ;
			} elsif ($class =~ /Euk/) {
				$cpfam = "e_"."$pfam" ;
			} elsif ($class =~ /NA/) {
				$cpfam = "n_"."$pfam" ;
			} elsif ($class =~ /Virus/) {
				$cpfam = "v_"."$pfam" ;
			} else {
				print "$id ($pfam) has no class\n" ;  <STDIN> ;
			}
			
			print "$class\t$pfam\t$l\t$cpfam\n" ; #<STDIN> ;
			
			
			$txpf{$cpfam} = 1 ;
			
			$len{$cpfam} += $l ;
			$tot{$cpfam} ++ ;	
			
			unless ($min{$cpfam}) {
				$min{$cpfam} = 1000000000000 ;
			}
		
			unless ($max{$cpfam}) {
				$max{$cpfam} = 0 ;
			}
		
			if ($l < $min{$cpfam}) {
				$min{$cpfam} = $l ;
		#		print "MIN $gene: $l\n" ; #<STDIN> ;
			}
			
			if ($l > $max{$cpfam}) {
				$max{$cpfam} = $l ;
		#		print "MAX $gene: $l\n" ; <STDIN> ;
			}
		}
		$/ = "\n" ;
	}


	my $out = "$mth"."-"."$genes"."_meanlen_PFAM.tab" ;
	open (OUT, ">$out") || die "Can't create $out\n" ;

	print     "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;
	print OUT "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;



	foreach my $cpfam (sort keys %txpf) {
		my $mean = ($len{$cpfam})/($tot{$cpfam}) ;
		my ($class, $pfam) = (split/\_/,$cpfam) ;
		my $name  = $names{$pfam} ;
		my $cname = "$class"."_"."$name" ;
		print OUT "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ;
		print     "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ; #<STDIN> ;
		#include names to sum different PFAMs into same gene
	}
}
