#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/ArtificialMetagenome" ;
#my $path = "/home/ana/LAB/Virus/ArtificialMg/cMessi/10ArtMg/Less/Annotation/RPKMs" ;

foreach my $melted (<$path/*_RPKMs_USCGsLarger_correctedMCPsSum.melted>) {
	my $fn = (split/\//,$melted)[-1] ;
	my $mt = (split/\_/,$fn)[0] ;
	open (M, "$melted") || die "Can't open $melted\n" ;
	<M> ;
	my (%cel, %vir, %mcps, %MG) ;
	while (<M>) {
		chomp ;
		my ($type, $mg, $class, $rpkms) = (split/\t/,$_)[0,2,3,4] ;
		$MG{$mg} = 1 ;
		if ($type eq "Ribosomal") {
			$cel{$mg} += $rpkms ;
		} elsif ($type eq "MCPs") {
			$mcps{$mg} += $rpkms ;
			if ($class eq "Virus") {
				$vir{$mg} += $rpkms ;
			}
		} 
		
	} close M ;
	
#	print "Metagenome\tMethod\tCells\tViralMCPs\tAllMCPs\n" ;
	foreach my $mg (keys %MG) {
		if ($cel{$mg}) {
			print "$mg\t$mt\tCells\t1\n"  ;
			if ($mcps{$mg}) {
				my $mcps  = $mcps{$mg}/$cel{$mg} ;
				print "$mg\t$mt\tAllMCPs\t$mcps\n"  ;
				if ($vir{$mg}) {
					my $viral = $vir{$mg}/$cel{$mg}  ;
					print "$mg\t$mt\tViralMCPs\t$viral\n"  ;
				#	my $mcps  = $mcps{$mg}/$cel{$mg} ;
				#	print "$mg\t$mt\t1\t$viral\t$mcps\n"  ;
				} else {
					print "$mg\t$mt\tViralMCPs\t0\n"  ;
				#	my $mcps  = $mcps{$mg}/$cel{$mg} ;
				#	print "$mg\t$mt\t1\t0\t$mcps\n"  ;
				}
			} else {
				print "$mg\t$mt\tAllMCPs\t0\n"  ;
			#	print "$mg\t$mt\t1\t0\t0\n"  ;
			}
		} else {
#			print "$mg\t$mt\tCells\t0\n"  ;
			if ($mcps{$mg}) {
#				print "$mg\t$mt\tAllMCPs\t$mcps{$mg}\n"  ;
				if ($vir{$mg}) {
#					print "$mg\t$mt\tViralMCPs\t$vir{$mg}\n"  ;
				#	my $viral = $vir{$mg}  ;
				#	my $mcps  = $mcps{$mg} ;
				#	print "$mg\t$mt\t0\t$viral\t$mcps\n"  ;
				} else {
#					print "$mg\t$mt\tViralMCPs\t0\n"  ;
				#	my $mcps  = $mcps{$mg} ;
				#	print "$mg\t$mt\t0\t0\t$mcps\n"  ;
				}
			} else {
#				print "$mg\t$mt\tAllMCPs\t0\n"  ;
			#	print "$mg\t$mt\t0\t0\t0\n"  ;
			}
		}
	}
}
