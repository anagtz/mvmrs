#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/ArtificialMg/cMessi/10ArtMg/Less/Annotation" ;
my $mcps = "MCPs.desc" ;
my %mcps ;

open (MCPS, "$mcps") || die "Can't open $mcps\n" ;
while (<MCPS>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($mcp, $desc) = (split/\t/,$_)[0,1] ;
	$mcps{$mcp} = $desc ;
}
my (%mg, %ev, %cp, %mgs) ;

#foreach my $hmm (<$path/Capsids/HMMoutFiles/*.hmmblout>) {
foreach my $hmm (<$path/HMMs/*/*_MCPs.hmmblout>) {
	if ($hmm =~ /HUH-Rep/) {
		print "Skipping $hmm as it is not a capsid\n" ;
		next ;
	} else {
		open (HMM, "$hmm") || die "Can't open $hmm\n" ;
		print "Keeping significant hits from $hmm\n"  ;
		my $fn  = (split/\//,$hmm)[-1] ;
		my $mth = (split/\//,$hmm)[11] ;
		my $mg  = (split/\_/,$fn)[0]   ;
		$mgs{$mg} = 1 ;
		print "$mg...\n" ; #<STDIN> ;
		
		while (<HMM>) {
			chomp ;
			next if ($_ =~ /\#/) ;
			my ($id, $capsid, $ev) = (split/ +/,$_)[0,2,4] ;
			next if ($capsid eq "Salasmaviridae_HK97-like") ;
			next if ($capsid eq "Aigarchaeota_HK97-like") ;
			next if ($capsid eq "Caulimoviridae_Retro-like") ;
			next if ($capsid eq "Chaseviridae_HK97-like") ;
			next if ($capsid eq "Pyrstoviridae_HK97-like") ;
			next if ($capsid =~ /HUH-Rep/) ;
			next unless ($mcps{$capsid}) ;
			if ($ev <= 1e-11) {
#				print "$mg\t$capsid\t$id\t$ev\n" ; # <STDIN> ;
				if ($ev{$id}) {
#					print "$mg\t$capsid\t$id\t$ev -- I already had this gene with a $ev{$id} evalue ($cp{$id})\n" ; # <STDIN> ;
					if ($ev < $ev{$id}) {
#						print "NEW VALUES for gene $id: $mg\t$capsid\t$ev\nI will overwrite this: previous evalue $ev{$id} previous capsid:$cp{$id}\n" ; <STDIN> ;
						$ev{$id} = $ev ;
						$cp{$id} = $capsid ;
						$mg{$id} = $mg ;
					} else {
#						print "..but previous values were better, so I do nothing\nNew e-value: $ev{$id} -- previous capsid: $cp{$id}\n" ; <STDIN> ;
					}
				} else {
					$ev{$id} = $ev ;
					$cp{$id} = $capsid ;
					$mg{$id} = $mg ;
				}
			}
		} close HMM ;
		
		if ($mth eq "OrfM") {
			my (%cgs, %one) ;
			foreach my $id (keys %ev) {
			#	my $contig = (split/\_/,$id)[0] ;
				my @pieces = (split/\_/,$id) ;
				pop @pieces ; ## Orf Number
				pop @pieces ; ## Frame Number
				pop @pieces ; ## Start Position
				my $contig  ;
				foreach my $i (@pieces) {
					$contig .= "$i"."_" ;
				}
				chop $contig ;
			#	print "***$id***<- $contig\n" ; <STDIN> ;
				if ($cgs{$contig}) {
					if ($ev{$cgs{$contig}} > $ev{$id}) {
					#	print "I will keep the new one $id: with a $ev{$id} evalue ($cp{$id})\t Old e-value: $ev{$cgs{$contig}}\n" ; <STDIN> ;
						$cgs{$contig} = $id ;
					} else {
					#	print "..but previous values were better, so I do nothing\nCurrent e-value: $ev{$id} -- previous (keeping): $ev{$cgs{$contig}}" ; <STDIN> ;
					}
				} else {
					$cgs{$contig} = $id ;
				}
			}
			
			foreach my $contig (keys %cgs) {
				my $id = $cgs{$contig} ;
			#	next unless ($id =~ /$mg/) ;
				$one{$id} = $contig    ;
			#	print "$id\t$contig\n" ; <STDIN> ;
			}
	
			print "Retrieving ORFs for $mg\n" ; #<STDIN> ;
			my $fna = "$path"."/OrfM/"."$mg"."_orfm.fna" ;
			my $one = "$mg"."_OrfM_MCPs.ffn" ;
			my $out = "$mg"."_OrfM_allMCPs.ffn" ;
			open (FNA, "$fna")  || die "Can't open $fna\n" ;
			open (OUT, ">$out") || die "Can't create $path/$out\n" ;
			open (ONE, ">$one") || die "Can't create $path/$one\n" ;
			$/ = "\n\>" ;
			while (<FNA>) {
				chomp ;
				my @lines = (split/\n/,$_) ;
				my $id    = shift @lines   ;
				$id =~ s/\>// ;
				my $seq ;
				if ($ev{$id}) {
					foreach my $i (@lines) {
						$seq .= $i ;
					}
					print OUT ">$id|$cp{$id}|$ev{$id}\n$seq\n" ; 
				#	print     ">$id|$cp{$id}|$ev{$id}\n$seq\n" ; <STDIN> ;
					
					if ($one{$id}) {
						print ONE ">$id|$cp{$id}|$ev{$id}|$one{$id}\n$seq\n" ;
					}
					
				}
			} close FNA ;
			$/ = "\n" ;
			
			print "Retrieving CDSs for $mg\n" ; #<STDIN> ;
			my $faa = "$path"."/OrfM/"."$mg"."_orfm.faa" ;
			my $one = "$mg"."_OrfM_MCPs.faa" ;
			my $out = "$mg"."_OrfM_allMCPs.faa" ;
			open (FAA, "$faa")  || die "Can't open $faa\n" ;
			open (OUT, ">$out") || die "Can't create $path/$out\n" ;
			open (ONE, ">$one") || die "Can't create $path/$one\n" ;
			$/ = "\n\>" ;
			while (<FAA>) {
				chomp ;
				my @lines = (split/\n/,$_) ;
				my $id    = shift @lines   ;
				$id =~ s/\>// ;
				my $seq ;
				if ($ev{$id}) {
					foreach my $i (@lines) {
						$seq .= $i ;
					}
					print OUT ">$id|$cp{$id}|$ev{$id}\n$seq\n" ; 
				#	print     ">$id|$cp{$id}|$ev{$id}\n$seq\n" ; <STDIN> ;
					
					if ($one{$id}) {
						print ONE ">$id|$cp{$id}|$ev{$id}|$one{$id}\n$seq\n" ;
					}
					
				}
			} close FAA ;
			$/ = "\n" ;
			
		} elsif ($mth eq "Prokka") {
			my (%cgs, %one) ;
			print "Getting one MCP per contig in $mg\n" ;
			my $gff = "$path"."/Prokka/"."$mg"."_prokka"."/"."$mg".".gff" ;
			open (GFF, "$gff") || die "Can't open $gff\n" ;
			while (<GFF>) {
				chomp ;
				next if ($_ =~ /^\#/) ;
				next if ($_ =~ /^\>/) ;
				next if ($_ =~ /^[ACGT]/) ;
				if ($_ =~ /Prodigal.*CDS/) {
				#	print "$_" ; <STDIN> ;
					my ($contig, $info) = (split/\t/,$_)[0,8] ;
					my $id = (split/\;/,$info)[0] ;
					$id =~ s/ID\=// ;
				#	print "$id...\n" ;
					if ($cp{$id}) {
				#		print "$id it's a capsid $cp{$id}\tCONTIG: $contig\n" ; <STDIN> ;
						if ($cgs{$contig}) {
				#			print "This contig already has a capsid protein $cgs{$contig}: $cp{$cgs{$contig}}\n" ;
							if ($ev{$cgs{$contig}} > $ev{$id}) {
				#				print "I will keep the new one $id: with a $ev{$id} evalue ($cp{$id})\t Old e-value: $ev{$cgs{$contig}}\n" ; <STDIN> ;
								$cgs{$contig} = $id ;
							} else {
				#				print "..but previous values were better, so I do nothing\nCurrent e-value: $ev{$id} -- previous (keeping): $ev{$cgs{$contig}}" ; <STDIN> ;
							}
						} else {
							$cgs{$contig} = $id ;
						}
					}
				}
			} close GFF ;
	
			foreach my $contig (keys %cgs) {
				my $id = $cgs{$contig} ;
			#	next unless ($id =~ /$mg/) ;
				$one{$id} = $contig    ;
				print "$id\t$contig\n" ;
			}
	
			print "Retrieving FFNs for $mg\n" ; #<STDIN> ;
			my $ffn = "$path"."/Prokka/"."$mg"."_prokka"."/"."$mg".".ffn" ;
			my $one = "$mg"."_Prokka_MCPs.ffn" ;
			my $out = "$mg"."_Prokka_allMCPs.ffn" ;
			open (FFN, "$ffn")  || die "Can't open $ffn\n" ;
			open (OUT, ">$out") || die "Can't create $path/$out\n" ;
			open (ONE, ">$one") || die "Can't create $path/$one\n" ;
			$/ = "\n\>" ;
			while (<FFN>) {
				chomp ;
				my @lines  = (split/\n/,$_) ;
				my $header = shift @lines   ;
				my @pieces = (split/ /,$header) ;
				my $id     = shift @pieces ;
				$id =~ s/\>// ;
				my $seq ;
				if ($ev{$id}) {
					foreach my $i (@lines) {
						$seq .= $i ;
					}
					my $fn ;
					foreach my $j (@pieces) {
						$fn .= "$j"." " ;
					} chop $fn ;
					
					print OUT ">$id|$cp{$id}|$ev{$id}|$fn\n$seq\n" ; 
#					print ">$id|$cp{$id}|$ev{$id}|$fn\n$seq\n" ; <STDIN> ;
					
					if ($one{$id}) {
						print ONE ">$id|$cp{$id}|$ev{$id}|$fn|$one{$id}\n$seq\n" ;
					}
					
				}
			} close FFN ;
			$/ = "\n" ;
			
			print "Retrieving FAAs for $mg\n" ; #<STDIN> ;
			my $faa = "$path"."/Prokka/"."$mg"."_prokka"."/"."$mg".".faa" ;
			my $one = "$mg"."_Prokka_MCPs.faa" ;
			my $out = "$mg"."_Prokka_allMCPs.faa" ;
			open (FAA, "$faa")  || die "Can't open $faa\n" ;
			open (OUT, ">$out") || die "Can't create $path/$out\n" ;
			open (ONE, ">$one") || die "Can't create $path/$one\n" ;
			$/ = "\n\>" ;
			while (<FAA>) {
				chomp ;
				my @lines  = (split/\n/,$_) ;
				my $header = shift @lines   ;
				my @pieces = (split/ /,$header) ;
				my $id     = shift @pieces ;
				$id =~ s/\>// ;
				my $seq ;
				if ($ev{$id}) {
					foreach my $i (@lines) {
						$seq .= $i ;
					}
					my $fn ;
					foreach my $j (@pieces) {
						$fn .= "$j"." " ;
					} chop $fn ;
					
					print OUT ">$id|$cp{$id}|$ev{$id}|$fn\n$seq\n" ; 
#					print ">$id|$cp{$id}|$ev{$id}|$fn\n$seq\n" ; <STDIN> ;
					
					if ($one{$id}) {
						print ONE ">$id|$cp{$id}|$ev{$id}|$fn|$one{$id}\n$seq\n" ;
					}
					
				}
			} close FAA ;
			$/ = "\n" ;
		} 
	}
}


