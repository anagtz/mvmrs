#!/usr/bin/perl -w

use strict ;

my $path  = "/home/ana/LAB/Virus/ArtificialMg/cMessi/10ArtMg/Less/Annotation" ;
my $uscgs = "USCGsMarkers.desc" ;
my %uscgs ;

open (USCG, "$uscgs") || die "Can't open $uscgs\n" ;
while (<USCG>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $desc) = (split/\t/,$_)[0,1] ;
	$uscgs{$pfam} = $desc ;
}

my $genes = (split/\./,$uscgs)[0] ;
foreach my $hmm (<$path/HMMs/*/V*M*_$genes.hmmblout>) {
	my $fn  = (split/\//,$hmm)[-1]      ;
	my $mth = (split/\//,$hmm)[11]      ;
	my $mg  = (split/\_/,$fn)[0]   ;
	my $out = "$mg"."_"."$mth"."_"."$genes".".fna" ;
	my ($fna, $faa) ; 
	if ($mth eq "Prokka") {
		$fna = "$path"."/Prokka/"."$mg"."_prokka/"."$mg".".ffn" ;
		$faa = "$path"."/Prokka/"."$mg"."_prokka/"."$mg".".faa" ;
	} elsif ($mth eq "OrfM") {
		$fna = "$path"."/OrfM/"."$mg"."_orfm.fna" ;
		$faa = "$path"."/OrfM/"."$mg"."_orfm.faa" ;
	}
	
	print "Metagenome: $mg\nMethod for ORF prediction: $mth\nOutput file: $out\nNucleotide fna file: $fna\n\n" ; #<STDIN> ;
	
	open (HMM, "$hmm")  || die "Can't open $hmm\n"   ;
	open (OUT, ">$out") || die "Can't create $out\n" ;
	open (FNA,  "$fna") || die "Can't open $fna\n"   ;
	
	my (%ribos) ;
	while (<HMM>) {
	chomp ;
		next if ($_ =~ /^\#/) ;
		my ($id, $name, $pfv) = (split/ +/,$_)[0,2,3] ;
#		print "$id\t$name\t$pfv\n" ; #<STDIN> ;
		my $pfam = (split/\./,$pfv)[0] ;
		if ($uscgs{$pfam}) {
			$ribos{$id} = $pfam ;
		}
	} close HMM ;


	$/ = "\n\>" ;
	while (<FNA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines   ;
		if ($head =~ /^\>/) {
			$head =~ s/\>// ;
		}
		
		my ($gene, $desc) ;
		if ($mth eq "Prokka") {
			my @words = (split/ /,$head) ;
			$gene = shift @words ;
			foreach my $word (@words) {
				$desc .= "$word " ;
			}
			chop $desc ;
		#	print "$gene\t$desc\n" ; #<STDIN> ;
		} elsif ($mth eq "OrfM") {
			$gene = $head ;
			$desc = $mth  ;
		}
		
		if ($ribos{$gene}) {
			my $seq ;
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			print OUT ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print "$desc\n" ;
		} else {
	#		print "$gene no match in $fna\n" ;
		} 
	} close FNA ;
	
	my $out = "$mg"."_"."$mth"."_"."$genes".".faa" ;
	open (OUT, ">$out") || die "Can't create $out\n" ;
	open (FAA,  "$faa") || die "Can't open $faa\n"   ;
	
	$/ = "\n\>" ;
	while (<FAA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines   ;
		if ($head =~ /^\>/) {
			$head =~ s/\>// ;
		}
		
		my ($gene, $desc) ;
		if ($mth eq "Prokka") {
			my @words = (split/ /,$head) ;
			$gene = shift @words ;
			foreach my $word (@words) {
				$desc .= "$word " ;
			}
			chop $desc ;
		#	print "$gene\t$desc\n" ; #<STDIN> ;
		} elsif ($mth eq "OrfM") {
			$gene = $head ;
			$desc = $mth  ;
		}
		
		if ($ribos{$gene}) {
			my $seq ;
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			print OUT ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print ">$gene|$ribos{$gene}|$desc\n$seq\n" ; #<STDIN> ;
	#		print "$desc\n" ;
		} else {
	#		print "$gene no match in $fna\n" ;
		} 
	}
	
	
	$/ = "\n" ;
	# retrieve nucleotide sequence
	# build files individually for mapping	
}
