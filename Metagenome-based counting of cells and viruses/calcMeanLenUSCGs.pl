#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;
my $desc = "$path"."/USCGs/USCGsMarkers.desc" ; 

open (DESC, "$desc") || die "Can't open $desc\n" ;
my $genes = "USCGsMarkers" ;

my %names ;

while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $name) = (split/\t/,$_) ;
	$names{$pfam} = $name ;
}

foreach my $fna (<$path/USCGs/*_USCGsMarkers.fna>) {
	my $fn = (split/\//,$fna)[-1]    ;
	my $mg = (split/\_USCGs/,$fn)[0] ;
	print "$mg...\n" ;
	
	
	my (%tot, %len, %min, %max, %txpf) ;
	open (FNA, "$fna") || die "Can't open $fna\n" ;
	$/ = "\n\>" ;
	while (<FNA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines ;
		$head =~ s/\>//g ;
			
		my ($id, $cpfam) = (split/\|/,$head)[0,1]  ;
		my $pfam = (split/\_/,$cpfam)[1] ;
		next unless ($names{$pfam}) ;
	#	print "$id, $cpfam\n" ; <STDIN> ;
		
		my $seq ;
		foreach my $i (@lines) {
			$seq .= $i ;
		}
			
		my $l = length($seq) ;
		
	#	print "$pfam\t$l\t$cpfam\n" ; <STDIN> ;
			
		$txpf{$cpfam} = 1 ;
			
		$len{$cpfam} += $l ;
		$tot{$cpfam} ++ ;	
			
		unless ($min{$cpfam}) {
			$min{$cpfam} = 1000000000000 ;
		}
		
		unless ($max{$cpfam}) {
			$max{$cpfam} = 0 ;
		}
		
		if ($l < $min{$cpfam}) {
			$min{$cpfam} = $l ;
	#		print "MIN $gene: $l\n" ; #<STDIN> ;
		}
			
		if ($l > $max{$cpfam}) {
			$max{$cpfam} = $l ;
	#		print "MAX $gene: $l\n" ; <STDIN> ;
		}
	}
	$/ = "\n" ;
#}


	my $out = "$mg"."_"."$genes"."_meanlen_PFAM.tab" ;
	open (OUT, ">$out") || die "Can't create $out\n" ;

	print     "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;
	print OUT "Gene\tPFAM\tMean Length\tMin Length\tMax Length\n" ;



	foreach my $cpfam (sort keys %txpf) {
		my $mean = ($len{$cpfam})/($tot{$cpfam}) ;
		my ($class, $pfam) = (split/\_/,$cpfam) ;
		my $name  = $names{$pfam} ;
		my $cname = "$class"."_"."$name" ;
		print OUT "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ;
		print     "$cname\t$pfam\t$mean\t$min{$cpfam}\t$max{$cpfam}\n" ; #<STDIN> ;
		#include names to sum different PFAMs into same gene
	}
}
