#!/usr/bin/perl -w

use strict ;


my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;
my $desc = "$path"."/MCPs/MCPs.desc" ; 

my (%names, %MCPs) ; 

open (DESC, "$desc") || die "Can't open $desc\n" ;
while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($name, $type) = (split/\t/,$_) ;
	if ($type eq "MCP") {
		$names{$name} = $type ;
		$MCPs{$name}  = 1 ;
#		print "$name\t$type\n" ; <STDIN> ;
	}
#	$desc{$name}  ++ ;
}
my $genes = "MCPs" ;

my $code = $ARGV[0] ;

unless ($code) {
	print "Metagenome code (e.g. DAL, LLA9, WC, etc...): " ;
	chomp ($code = <STDIN>) ;
}

my (%pmf, %metas) ;
my $tr = "$path"."/TotalReads/"."$code"."-totalReads.tab" ;
#my $tr = "SimulatedMetagenomeStats.tsv" ;
open (TR, "$tr") || die "Can't open $tr file. I need the total reads per sample to normalize by sequencing depth\n" ;
#<TR> ;
while (<TR>) {
	chomp ;
	my ($meta, $totReads) = (split/\t/,$_)[0,1] ;
	$metas{$meta} = 1 ;
#	$totReads	  =~ s/\,//g ;
	$pmf{$meta}   = $totReads/1000000 ; # This is my "per million" factor for each metagenome	
	print "$meta, $totReads:  $pmf{$meta}\n" ; #<STDIN> ;
}

my $orphans = "$code"."_"."$genes"."_Without_MappedReads.tab" ;
open (ORPH, ">$orphans") || die "Can't create $orphans\n" ;

my (%mean, %name, %min, %max)    ;
my (%rdCounts, %class, %rcid)    ;
my (%lens, %CPF, %genes, %GnsMg) ;

#my @mthds = qw(OrfM) ;
#my @mthds = qw(Prokka OrfM) ;
#foreach my $code (@mthds) {
foreach my $meta (keys %metas) {
	
	print "$meta...\n" ;
	my $means = "$path"."/MCPs/Tables_Viral_Prophages_MCPs/"."$meta"."_meanlen_MCPs.tab" ; 
	open (MEAN, "$means") || die "Can't open $means\n" ;
	<MEAN> ;
	while (<MEAN>) {
		chomp ;
		my ($mcp, $mean, $min, $max) = (split/\t/,$_) ;
	
#		next if ($pfam eq "PF13479") ;
#		next if ($pfam eq "PF06616") ;
		
#		my $olc   = (split/\_/,$cname)[0] ;
#		my $cpfam = "$olc"."_"."$pfam" ;
		
		$mean{$mcp} = $mean ;
#		$name{$cpfam} = $cname ;
		
		$min{$mcp}  = $min  ;
		$max{$mcp}  = $max  ;
		
#		print "$cname\t$cpfam\t$mean\n" ; <STDIN> ;
	} close MEAN ;


	
#	my (%rdCounts, %class, %rcid) ;
	my $lens = "$path"."/MCPs/Tables_Viral_Prophages_MCPs/"."$code"."_length_"."$genes".".tab" ;
	
	open (LEN, ">$lens") || die "Can't create $lens\n" ;

#	my $suf = "$code"."_"."$genes"."_TaxaGroup.tab" ;
#	foreach my $txgp (<Taxa/V*M*_$suf>) {
	my (%TX) ;
#		my $fn = (split/\//,$txgp)[-1] ;
#		my $mg = (split/\_/,$fn)[0]   ;
#		open (TX, "$txgp") || die "Can't open $txgp\n" ;
#		while (<TX>) {
#			chomp ;
#			my ($gene, $group) = (split/\t/,$_)[0,2]    ;
#			my ($id, $mcp)    = (split/\|/,$gene)[0,1] ;
#			next unless ($names{$mcp}) ;
#			$TX{$id} = $group ;
#		} close TX ;
		
	my $faa = "$path"."/"."$genes"."/Seqs/"."$meta"."_viral_prophage_"."$genes".".ffn" ; ## These are already one MCP per contig and splitted in Viral or Prophage
#	foreach my $faa (<USCGs/V*M*_$code*_USCGsMarkers.fna>) {
	$/ = "\n\>" ;
	open (FAA, "$faa") || die "Can't open $faa\n" ;
#	<FAA> ;
	while (<FAA>) {
		chomp ;
		my ($header, $seq) = (split/\n/,$_)[0,1]   ;
		if ($header =~ /\>/) {
			$header =~ s/\>//g ;
		}
		my ($id, $mcp, $tx) = (split/\|/,$header)[0,1,2] ;
		next unless ($names{$mcp}) ;

		my $len = length $seq ;
		my ($cMcp)  ;
			
		if ($tx =~ /Prophage/) {
			$cMcp = "p_"."$mcp" ;
		} elsif ($tx =~ /Virus/) {
			$cMcp = "v_"."$mcp" ;
		} else {
			print "$id ($mcp) has no class\n" ;  <STDIN> ;
		}
	#	print     "$id\t$cpfam\t$len\n" ; #<STDIN> ;
		print LEN "$id\t$cMcp\t$len\n" ; #<STDIN> ;
		$lens{$id}  = $len  ;
		$genes{$id} = $cMcp ;
		$CPF{$cMcp} = 1     ;
		$GnsMg{$id} = $meta ;
		$TX{$id}    = $tx   ;
	} close FAA ;
	$/ = "\n" ;
#	}
	
#	my (%rdCounts, %cov, %control, %seen, %class, %rcid) ;
	my (%control) ;
	my $mreadf = "$path"."/MCPs/MappedReads/"."$meta"."_"."$genes".".mapped.sam" ;
#	foreach my $mreadf (<MappedReads/$code*_USCGsMarkers.mapped.sam>) {
#	my $pmeta = (split/\_USCG/,$mreadf)[0] ;
#	my $meta  = (split/\//,$pmeta)[1] ;
	#print "$meta\n" ;  <STDIN> ;
	open (MAP, "$mreadf") || die "Can't open $mreadf\n" ;
	while (<MAP>) {
		chomp ;
		my ($read, $gene) = (split/\t/,$_)[0,2] ;
	#	print "$read\t$gene\t" ;  <STDIN> ;
	#	my ($cpfam, $name) = (split/\|/,$gene)[1,2] ;
		
		my ($id, $mcp) = (split/\|/,$gene)[0,1] ;
		my $cMCP = $genes{$id}   ;
		unless ($cMCP) {
			print "$id still without a C_MCP\n" ; <STDIN> ;
		}
		next unless $names{$mcp} ;
#		print "$read\t$gene\t*$cpfam*\n" ;  #<STDIN> ;
		if ($CPF{$cMCP}) {
			
		#	$CPF{$cpfam} = 1 ;
			
			unless ($TX{$id}) {
			#	print "$id is not in TX hash\n" ; <STDIN> ;
			#	$noTx{$id}  = 1    ;
				$TX{$id}    = "NA" ;
			}
			
			$class{$TX{$id}} = 1 ;
			
			if ($control{$read}) {
#				print "$read already has a $control{$read}\nNOW: $cpfam\n" ; <STDIN> ;
				## I just count reads once
#				print "\n" ;
			} else {
				$control{$read} = $cMCP ;
			#	print "$read\t-->$id<--\t$cpfam\t$mg\n" ; <STDIN> ;
				
				$rdCounts{$meta}{$cMCP} ++  ;
				$rcid{$meta}{$id} ++ ;
				
#				print "$meta\t$cname\t$rdCounts{$meta}{$cname}\n" ; #<STDIN> ;
				
				## Change calcMeanLen script to sum those ribosomal genes that are the same but have different PFAMs
				## Calculate abundance (read coverage) and normalize by mean length for each gene
			}
		} else {
			print "\nWARNING: $cMCP is not in $means --> $id --- $mcp\n" ; 
			 <STDIN> ;
		#	These PFAMs will be excluded. This condition should not be met....
		}
	} close MAP ;
} ## foreach MG from code
#}

############ COVERAGE ##########

#my $sum  = "$code"."_"."$genes"."_Sum_RPKM_indv.tab" ;
#my $avg  = "$code"."_"."$genes"."_Avg_RPKM_indv.tab" ;
my $MCPs = "$code"."_"."$genes"."_RPKM_indv.tab" ;
#open (AVG, ">$avg") || die "Can't create $avg\n" ;
#open (SUM, ">$sum") || die "Can't create $sum\n" ;
open (PF, ">$MCPs") || die "Can't create $MCPs\n" ;

my $header = "Gene\tClass\t" ;
foreach my $meta (sort keys %metas) {
	if ($meta =~ /Sal6/) {
		$meta =~ s/Sal6/Sal06/ ;
	}
	$header .= "$meta\t" ;
}
chop $header ;
print "$header\n" ;
print PF   "$header\n" ;
#print SUM  "$header\n" ;
#print AVG  "$header\n" ;


my %sum ;
	
	
my $all = "$code"."_"."$genes"."_RPKMperGene.tab" ; 
open (ALL, ">$all") || die "Can't create $all\n" ;

foreach my $id (keys %genes) {
	my $mcp ;
	my $cMCP   = $genes{$id} ;
	my @fields = (split/\_/,$cMCP) ;
	my $class  = shift @fields     ;
	foreach my $j (@fields) {
		$mcp .= "$j"."_" ;
	} chop $mcp ;
	print "$id\t$cMCP\n" ;


	unless ($mcp) {
		print "No capsid? $cMCP - $id ?" ; <STDIN> ;
	}

	
	my $taxon ;
	if ($class eq "p") {
		$taxon = "Prophage" ;
	} elsif ($class eq "v") {
		$taxon = "Virus" ;
	}

#	my $name   = $name{$cpfam} ;


	my $meta = $GnsMg{$id} ;	

	#my $meta  = (split/\_\d{3}\d+/,$id)[0] ;
	#print "$meta -> META from ID $id\n" ; <STDIN> ;
	
#	if ($class eq "d") {
	#	print "$meta\t $cpfam\n" ; <STDIN> ;
	#}

	if ($rcid{$meta}{$id}) {
	print "RPKMs for each gene\n" ;
		my $readCounts = $rcid{$meta}{$id} ; print "Read Counts for $id: $readCounts\n" ;
		my $RPM        = $rcid{$meta}{$id} / $pmf{$meta} ; # Reads Per Million
												## Dividing the read counts by the “per million” 
												## scaling factor, normalizes for sequencing depth, 
												## giving you reads per million (RPM)
			## Divide the RPM values by the length of the gene, in kilobases. This gives you RPKM.
			#my $meanKB     = $mean{$name{$pfam}} / 1000 ; 
		print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
		my $len        = $lens{$id} / 1000 ;
		my $RPKM       = $RPM / $len ;
		print "Divided by the length of the gene in Kb: $lens{$id} = $RPKM\n\n" ; #<STDIN> ;
#		print "len: $len\tRPKM: $RPKM\n" ; <STDIN> ;
	
		
	#	if ($names{$pfam}) {
	#		my $cdesc = "$class"."_"."$names{$pfam}" ;
	#		$sum{$meta}{$cdesc} += $RPKM ;
		#	print "SUM - $meta - $cdesc += $RPKM" ; <STDIN> ;
	#	} else {
	#		print "WARNING: $pfam! $desc\t$names{$pfam}*\n" ; <STDIN> ;
	#	}
	
		print "(line 283): $id\t$meta\t$code\t$mcp\t$taxon\t$RPKM\n" ; #<STDIN> ;
		print ALL         "$id\t$meta\t$code\t$mcp\t$taxon\t$RPKM\n" ;
	} else {
		print "WARNING: $id is empty in \"rcid\" hash! ($meta)\n" ; #<STDIN> ;
		print ORPH "$id\t$meta\t$code\t$mcp\t$taxon\n" ;
	}
}		
		
#foreach my $meta (sort keys %metas) {
#	print "$meta\n" ; <STDIN> ;
foreach my $cMCP (keys %CPF)   {

	my $mcp ;
	my @fields = (split/\_/,$cMCP) ;
	my $class  = shift @fields     ;
	foreach my $j (@fields) {
		$mcp .= "$j"."_" ;
	} chop $mcp ;

	my $taxon ;
	if ($class eq "p") {
		$taxon = "Prophage" ;
	} elsif ($class eq "v") {
		$taxon = "Virus" ;
	}
#	my $name   = $name{$cpfam} ;

	my $rpkml = "$mcp\t$taxon\t"  ;
	
	foreach my $meta (sort keys %metas) {
	print "$meta\n" ; #<STDIN> ;	

		print "RPKMs for each MCP\n" ;
		if ($rdCounts{$meta}{$cMCP}) {
		
			my $readCounts = $rdCounts{$meta}{$cMCP} ; 				print "Read Counts for $cMCP: $readCounts\n" ;
			my $RPM        = $rdCounts{$meta}{$cMCP} / $pmf{$meta} ;	print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
			my $RPKM ;
			if ($mean{$mcp}) {
			
				my $meanKB = $mean{$mcp} / 1000 ;
				$RPKM      = $RPM / $meanKB ;							print "Divided by the mena length of the Pfams for that Taxa category: $meanKB = $RPKM\n" ; #<STDIN> ;
				
			} else {
				$RPKM = 0 ;												print "$cMCP was not present for that Taxa in the $meta metagenome, hence: RPKM = 0\n" ; <STDIN> ;
			}
		#	my $cdesc = "$class"."_"."$names{$mcp}" ;
			$rpkml .= "$RPKM\t" ;
		} else {
			$rpkml .= "0\t" ;
		}
		#	chop $rpkml ;
		#	print "**$rpkml**\n" ; <STDIN> ;
		#	print PF "$rpkml\n" ;
	}
	chop $rpkml ;
	print "**$rpkml**\n" ; # <STDIN> ;
	print PF "$rpkml\n" ;
}



