#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;

foreach my $faa (<$path/MCPs/Seqs/T*_Prokka_MCPs.faa>) {
	my $fn = (split/\//,$faa)[-1]   ;
	my $mg = (split/\_Prok/,$fn)[0] ;
	print "$mg...\n" ;
	my (%mcps, %ev, %seq) ;
	open (FAA, "$faa") || die "Can't open $faa\n" ;
	$/ = "\n\>" ;
	while (<FAA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines   ;
		
		if ($head =~ /^\>/) {
			$head =~ s/\>//g ;
		}
	#	print "$head\n" ; <STDIN> ;
		my ($id, $mcp, $ev) = (split/\|/,$head)[0,1,2] ;
		
		
		my $seq ;
		foreach my $l (@lines) {
			$seq .= $l ;
		}
		$mcps{$id} = $mcp ;
		$ev{$id}   = $ev  ;
		$seq{$id}  = $seq ;
		
	} close FAA ; 
	$/ = "\n" ;
	
	my $out  = "$path"."/MCPs/Seqs/"."$mg"."_CellularMCPs.faa" ;
	my $txgp = "$path"."/MCPs/Taxa/"."$mg"."_TaxaGroup.tab"    ;
	open (TX, "$txgp") || die "Can't open $txgp\n"  ;
	open (FA, ">$out") || die "Can't create $out\n" ;
	while (<TX>) {
		chomp ;
		my ($id, $class) = (split/\t/,$_)[0,2] ;
		if ($id =~ /HSSLS-CSL3KL/) {
			$id =~ s/CSL3KL/CSL3KL_/g ;
		}
#		print "$id\n" ; <STDIN> ;
		if ($mcps{$id}) {
			unless ($class eq "Virus") {
			#	print    ">$id|$mcps{$id}|$class|$ev{$id}\n$seq{$id}\n" ; <STDIN> ;
				print FA ">$id|$mcps{$id}|$class|$ev{$id}\n$seq{$id}\n" ;
			}
		}
	} close TX ;
}
