#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;


## Get index with all MCPs (one per contig) and store it's e-value
## Store which of these are cellular
## open cellMCPs_Encapsulins see if they also match to an encapsulin and if so, with which e-value. -> encapsulins
## no match: prophages
## rest: viral
## print their FNA for mapping and then RPKMs.

foreach my $MCPs (<$path/MCPs/Seqs/T*_Prokka_MCPs.faa>) {
	my $fn = (split/\//,$MCPs)[-1]    ;
	my $mg = (split/\_Prokka/,$fn)[0] ;
	print "$mg...\n" ;
	
	my (%mcps, %mev) ;
	open (FAA, "$MCPs") || die "Can't open $MCPs\n" ;
	while (<FAA>) {
		chomp ;
		next unless ($_ =~ /^\>/) ;
		my ($id, $mcp, $ev) = (split/\|/,$_)[0,1,2] ;
		$id =~ s/\>//g ;
		$mcps{$id} = $mcp ;
		$mev{$id}  = $ev  ;
	} close FAA ;
	
	my %cMCPs ;
	my $cell = "$path"."/MCPs/Seqs/"."$mg"."_CellularMCPs.faa" ;
	open (FAA, "$cell") || die "Can't open $cell\n" ;
	while (<FAA>) {
		chomp ;
		next unless ($_ =~ /^\>/) ;
		my ($id) = (split/\|/,$_)[0] ;
		$id =~ s/\>//g ;
		$cMCPs{$id} = 1 ;
	} close FAA ;
	
	my (%ecp, %eev) ;
	my $encap = "$path"."/MCPs/HMMs/"."$mg"."_Prokka_CellMCPs_Encapsulins.hmmblout" ;
	open (HMM, "$encap") || die "Can't open $encap\n" ;
	while (<HMM>) {
		chomp ;
		next if ($_ =~ /^\#/) ;
		my ($id,  $ecp, $ev)  = (split/ +/,$_)[0,2,4]  ;
		my ($gid) = (split/\|/,$id)[0] ;
		next if ($ev eq "0") ;
		if ($ev <= 1e-11) {
			if ($eev{$gid}) {
#				print "$mg\t$capsid\t$id\t$ev -- I already had this gene with a $ev{$id} evalue ($cp{$id})\n" ; # <STDIN> ;
				if ($ev < $eev{$gid}) {
#					print "NEW VALUES for gene $id: $mg\t$capsid\t$ev\nI will overwrite this: previous evalue $ev{$id} previous capsid:$cp{$id}\n" ; <STDIN> ;
					$eev{$gid} = $ev  ;
					$ecp{$gid} = $ecp ;
				} else {
#					print "..but previous values were better, so I do nothing\nNew e-value: $ev{$id} -- previous capsid: $cp{$id}\n" ; <STDIN> ;
				}
			} else {
				$eev{$gid} = $ev  ;
				$ecp{$gid} = $ecp ;
			}
		}
	} close HMM ;
	
	my %seqs ;
	my $info = "$mg"."_MCPs.tab" ;
	open (OUT, ">$path/MCPs/Tables_Viral_Prophages_MCPs/$info") || die "Can't create $path/MCPs/Tables_Viral_Prophages_MCPs/$info\n" ;
	foreach my $id (keys %mcps) {
		if ($ecp{$id}) {
		#	print "$id: Encapsulin ($eev{$id}) vs. MCP ($mev{$id})\n" ;
			if ($eev{$id} < $mev{$id}) {
				print     "$id\tEncapsulin\t$ecp{$id}\t$eev{$id}\tEncapsulin\n" ;
				print OUT "$id\tEncapsulin\t$ecp{$id}\t$eev{$id}\tEncapsulin\n" ;
		#		print "Will classify it as an Encapsulin\n" ; <STDIN> ;
			} else {
				$seqs{$id} = "Prophage" ;
				print     "$id\tMCP\t$mcps{$id}\t$mev{$id}\tProphage\n" ;
				print OUT "$id\tMCP\t$mcps{$id}\t$mev{$id}\tProphage\n" ;
		#		print "Will classify it as a prophage (cellular MCP)\n" ; <STDIN> ;
			}
		}  else {
			## gene is a MCP and not an encapsulin
			$seqs{$id} = "Virus" ;
			print     "$id\tMCP\t$mcps{$id}\t$mev{$id}\tVirus\n" ;
			print OUT "$id\tMCP\t$mcps{$id}\t$mev{$id}\tVirus\n" ;
		}
	}
	
	my $ffn = "$path"."/FNA_seqs/"."$mg".".ffn" ;
	my $out = "$path"."/MCPs/Seqs/"."$mg"."_viral_prophage_MCPs.ffn" ;
	open (MCP, ">$out") || die "Can't create $out\n" ;
	open (FFN, "$ffn")  || die "Can't open $ffn\n"   ;
	$/ = "\n\>" ;
	while (<FFN>) {
		chomp ;
		my @lines  = (split/\n/,$_) ;
		my $header = shift @lines   ;
		my @pieces = (split/ /,$header) ;
		my $id     = shift @pieces ;
		$id =~ s/\>// ;
		if ($id =~ /HSSLS-CSL3KL/) {
			$id =~ s/CSL3KL/CSL3KL_/g ;
		}
		my $seq ;
		if ($seqs{$id}) {
			foreach my $i (@lines) {
				$seq .= $i ;
			}
			my $fn ;
			foreach my $j (@pieces) {
				$fn .= "$j"." " ;
			} chop $fn ;
			
			print MCP ">$id|$mcps{$id}|$seqs{$id}|$mev{$id}|$fn\n$seq\n" ; 
		#	print     ">$id|$mcps{$id}|$seqs{$id}|$mev{$id}|$fn\n$seq\n" ; <STDIN> ;
			
				
		}
	} close FFN ;
	$/ = "\n" ;
}
