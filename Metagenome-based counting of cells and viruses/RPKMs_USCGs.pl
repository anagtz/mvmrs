#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;
my $desc = "$path"."/USCGs/USCGsMarkers.desc" ; 

my (%names, %desc) ;
my $genes = "USCGsMarkers" ;

open (DESC, "$desc") || die "Can't open $desc\n" ;
while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my ($pfam, $name) = (split/\t/,$_)[0,1] ;
	$names{$pfam} = $name ;
	$desc{$name}  ++ ;
	print "$pfam, $name\n" ;
}

my $code = $ARGV[0] ;

unless ($code) {
	print "Metagenome code (e.g. DAL, LLA9, WC, etc...): " ;
	chomp ($code = <STDIN>) ;
}

my (%pmf, %metas) ;


my $tr = "$path"."/TotalReads/"."$code"."-totalReads.tab" ;
open (TR, "$tr") || die "Can't open $tr file. I need the total reads per sample to normalize by sequencing depth\n" ;
#print "$tr\n" ;
#<TR> ;
while (<TR>) {
	chomp ;
	my ($meta, $totReads) = (split/\t/,$_)[0,1] ;
	$metas{$meta} = 1 ;
#	$totReads	  =~ s/\,//g ;
	$pmf{$meta}   = $totReads/1000000 ; # This is my "per million" factor for each metagenome	
#	print "$meta, $totReads:  $pmf{$meta}\n" ;
}

my $orphans = "$code"."_"."$genes"."_Without_MappedReads.tab" ;
open (ORPH, ">$orphans") || die "Can't create $orphans\n" ;


my (%lens, %CPF, %genes) ;
my (%rdCounts, %class, %rcid) ;
my (%mean, %min, %max, %name) ;
#my %GnsMg ;

foreach my $meta (keys %metas) {
	
	my $means = "$path"."/RPKMs/"."$meta"."_"."$genes"."_meanlen_PFAM.tab" ; 
	open (MEAN, "$means") || die "Can't open $means\n" ;
#	<MEAN> ;
	while (<MEAN>) {
		chomp ;
		my ($cname, $pfam, $mean, $min, $max) = (split/\t/,$_) ;
	
#		next if ($pfam eq "PF13479") ;
#		next if ($pfam eq "PF06616") ;
		
		my $olc   = (split/\_/,$cname)[0] ;
		my $cpfam = "$olc"."_"."$pfam" ;
		
		$mean{$cpfam} = $mean ;
		$name{$cpfam} = $cname ;
		
		$min{$cpfam}  = $min  ;
		$max{$cpfam}  = $max  ;
		
#		print "$cname\t$cpfam\t$mean\n" ; <STDIN> ;
	} close MEAN ;


	my $lens = "$meta"."-"."$genes"."_length_PFAM.tab" ;
	
	open (LEN, ">$lens") || die "Can't create $lens\n" ;

	my $faa = "$path"."/USCGs/"."$meta"."_"."$genes".".fna" ;
	$/ = "\n\>" ;
	open (FAA, "$faa") || die "Can't open $faa\n" ;
	<FAA> ;
	while (<FAA>) {
		chomp ;
		my ($header, $seq) = (split/\n/,$_)[0,1]   ;
		$header =~ s/\>//g ;
		my ($id, $cpfam) = (split/\|/,$header)[0,1] ;
		my $pfam = (split/\_/,$cpfam)[1] ;
		next unless ($names{$pfam}) ;
		
		if ($id =~ /PBS/) {
			$id =~ s/PBS/LSSM/ ;
		} elsif ($id =~ /HSSLS-CSL3KL/) {
			$id =~ s/HSSLS-CSL3KL/HSSLS-CSL3KL_/ ;
		}
		
		my $len = length $seq ;
#		print     "$id\t$cpfam\t$len\n" ; <STDIN> ;
		print LEN "$id\t$cpfam\t$len\n" ; #<STDIN> ;
		$lens{$id}   = $len ;
		$genes{$id}  = $cpfam ;
		$CPF{$cpfam} = 1 ;
#		$GnsMg{$id}  = $meta ;
	} close FAA ;
	$/ = "\n" ;


	my (%control) ;
	my $mreadf = "$path"."/USCGs/MappedReads/"."$meta"."_USCGsMarkers.mapped.sam" ;
	print "Counting reads from ...$meta\n" ; # <STDIN> ;
	open (MAP, "$mreadf") || die "Can't open $mreadf\n" ;
	while (<MAP>) {
		chomp ;
		my ($read, $gene) = (split/\t/,$_)[0,2] ;
	#	print "$read\t$gene\t" ;  <STDIN> ;

		my ($id, $cpfam) = (split/\|/,$gene)[0,1] ;
		
		if ($id =~ /PBS/) {
			$id =~ s/PBS/LSSM/ ;
		} elsif ($id =~ /HSSLS-CSL3KL/) {
			$id =~ s/HSSLS-CSL3KL/HSSLS-CSL3KL_/ ;
		}
		
	#	print "*$cpfam*------$id!\n" ; <STDIN> ;
		my $pfam = (split/\_/,$cpfam)[1] ;
		next unless $names{$pfam} ;
		unless ($cpfam) {
			print "$id still without a C_Pfam\n" ; <STDIN> ;
		}
	#	print "$read\t$gene\t*$cpfam*\n" ;  #<STDIN> ;
		if ($CPF{$cpfam}) {
				
		#	$CPF{$cpfam} = 1 ;
		#	print "$pfam $cpfam $id!\n" ; <STDIN> ;
				
			if ($control{$read}) {
	#			print "$read already has a $control{$read}\nNOW: $cpfam\n" ; <STDIN> ;
				## I just count reads once
	#			print "\n" ;
			} else {
				$control{$read} = $cpfam ;
	#			print "$read\t-->$id<--\t$cpfam\t$meta\n" ; #<STDIN> ;
					
				$rdCounts{$meta}{$cpfam} ++  ;
				$rcid{$meta}{$id} ++ ;
					
	#			print "$meta\t$cpfam\t$rdCounts{$meta}{$cpfam}\n" ; <STDIN> ;
					
				## Change calcMeanLen script to sum those ribosomal genes that are the same but have different PFAMs
				## Calculate abundance (read coverage) and normalize by mean length for each gene
			}
		} else {
			print "\nWARNING: $cpfam is not in $means --> $id --- $pfam\n" ; 
			 <STDIN> ;
		#	These PFAMs will be excluded. This condition should not be met....
		}
	} close MAP ;
} ## foreach metagenome
#}

############ COVERAGE ##########

my $PFAM = "$code"."_"."$genes"."_PFAM_RPKM_indv.tab" ;
open (PF, ">$PFAM") || die "Can't create $PFAM\n" ;

my $header = "Gene\tClass\t" ;
foreach my $meta (sort keys %metas) {
	if ($meta =~ /Sal6/) {
		$meta =~ s/Sal6/Sal06/ ;
	}
	$header .= "$meta\t" ;
}
chop $header ;
print "$header\n" ;
print PF   "$header\n" ;


my %sum ;
	

my $all = "$code"."_"."$genes"."_PFAM_RPKMperGene.tab" ; 
open (ALL, ">$all") || die "Can't create $all\n" ;

foreach my $id (keys %genes) {
	
	
	
	my $cpfam = $genes{$id} ;
	my ($class, $pfam) = (split/\_/,$cpfam) ;
	print "$id\t$cpfam\n" ; # <STDIN> ;
	
	
	unless ($pfam) {
		print "No PFAM? $cpfam ?" ; <STDIN> ;
	}
	

	my $taxon ;
	if ($class eq "a") {
		$taxon = "Archaea_FL" ;
	} elsif ($class eq "b") {
		$taxon = "Bacteria_FL" ;
	} elsif ($class eq "c") {
		$taxon = "CPR" ;
	} elsif ($class eq "d") {
		$taxon = "DPANN" ;
	} elsif ($class eq "e") {
		$taxon = "Eukarya" ;
	} elsif ($class eq "n") {
		$taxon = "NotAssigned" ;
	} elsif ($class eq "v") {
		$taxon = "Virus" ;
	}
	
	my $name   = $name{$cpfam} ;

	my $meta = (split/\_\d{5}/,$id)[0] ;
#	my $meta = $GnsMg{$id} ;
#	print "$meta!\n" ; <STDIN> ;
	
#		if ($noTx{$id}) {
#			print NOTX "$id\t$meta\t$code\t$names{$pfam}\t$taxon\n" ;
#		}
	
	if ($rcid{$meta}{$id}) {
		print "RPKMs for each gene\n" ;
		my $readCounts = $rcid{$meta}{$id} ; print "Read Counts for $id: $readCounts\n" ;
		my $RPM        = $rcid{$meta}{$id} / $pmf{$meta} ; # Reads Per Million
													## Dividing the read counts by the “per million” 
													## scaling factor, normalizes for sequencing depth, 
													## giving you reads per million (RPM)
		## Divide the RPM values by the length of the gene, in kilobases. This gives you RPKM.
		#my $meanKB     = $mean{$name{$pfam}} / 1000 ; 
		print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
		my $len        = $lens{$id} / 1000 ;
		my $RPKM       = $RPM / $len ;
		print "Divided by the length of the gene in Kb: $lens{$id} = $RPKM\n\n" ; #<STDIN> ;
#		print "len: $len\tRPKM: $RPKM\n" ; <STDIN> ;
		
		
		#	if ($names{$pfam}) {
		#		my $cdesc = "$class"."_"."$names{$pfam}" ;
		#		$sum{$meta}{$cdesc} += $RPKM ;
			#	print "SUM - $meta - $cdesc += $RPKM" ; <STDIN> ;
		#	} else {
		#		print "WARNING: $pfam! $desc\t$names{$pfam}*\n" ; <STDIN> ;
		#	}
		
	#	print "(line 306): $id\t$meta\t$code\t$names{$pfam}\t$taxon\t$RPKM\n" ; <STDIN> ;
		print ALL  "$id\t$meta\t$code\t$names{$pfam}\t$pfam\t$taxon\t$RPKM\n" ;
	} else {
		print "WARNING: $id is empty in \"rcid\" hash! ($meta)\n" ; #<STDIN> ;
		print ORPH "$id\t$meta\t$code\t$names{$pfam}\t$taxon\n" ;
	}
}		
		
	#foreach my $meta (sort keys %metas) {
	#	print "$meta\n" ; <STDIN> ;
foreach my $cpfam (keys %CPF)   {
	
	my ($class, $pfam) = (split/\_/,$cpfam) ;
	
	my $taxon ;
	if ($class eq "a") {
		$taxon = "Archaea_FL" ;
	} elsif ($class eq "b") {
		$taxon = "Bacteria_FL" ;
	} elsif ($class eq "c") {
		$taxon = "CPR" ;
	} elsif ($class eq "d") {
		$taxon = "DPANN" ;
	} elsif ($class eq "e") {
		$taxon = "Eukarya" ;
	} elsif ($class eq "n") {
		$taxon = "NotAssigned" ;
	} elsif ($class eq "v") {
		$taxon = "Virus" ;
	}
	my $name   = $name{$cpfam} ;
	
	my $rpkml = "$pfam\t$taxon\t"  ;
	
	foreach my $meta (sort keys %metas) {
		print "$meta\n" ; #<STDIN> ;	

		print "RPKMs for each Pfam\n" ;
		if ($rdCounts{$meta}{$cpfam}) {
		
			my $readCounts = $rdCounts{$meta}{$cpfam} ; 				print "Read Counts for $cpfam: $readCounts\n" ;
			my $RPM        = $rdCounts{$meta}{$cpfam} / $pmf{$meta} ;	print "Divided by the per million factor: $pmf{$meta} = $RPM\n" ;
			my $RPKM ;
			if ($mean{$cpfam}) {
			
				my $meanKB = $mean{$cpfam} / 1000 ;
				$RPKM      = $RPM / $meanKB ;							print "Divided by the mean length of the Pfams for that Taxa category: $meanKB = $RPKM\n" ; #<STDIN> ;
			
			} else {
				$RPKM = 0 ;												print "That Pfam was not present for that Taxa in the $meta metagenome, hence: RPKM = 0\n" ; <STDIN> ;
			}
			my $cdesc = "$class"."_"."$names{$pfam}" ;
			$rpkml .= "$RPKM\t" ;
		} else {
			$rpkml .= "0\t" ;
		}
		#	chop $rpkml ;
		#	print "**$rpkml**\n" ; <STDIN> ;
		#	print PF "$rpkml\n" ;
	}
	chop $rpkml ;
	print "**$rpkml**\n" ; # <STDIN> ;
	print PF "$rpkml\n" ;
}



