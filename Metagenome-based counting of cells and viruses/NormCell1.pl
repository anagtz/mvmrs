#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka/RPKMs" ;
my $code = $ARGV[0] ;

unless ($code) {
	print "Metagenome code (e.g. DAL, LLA9, WC, etc...): " ;
	chomp ($code = <STDIN>) ;
}

my $melted = "$path"."/"."$code"."_RPKMs_USCGsLarger_correctedMCPsSum.melted" ;
open (M, "$melted") || die "Can't open $melted\n" ;
<M> ;
my (%cel, %vir, %mcps, %MG, %phg) ;
while (<M>) {
	chomp ;
	my ($type, $mg, $class, $rpkms) = (split/\t/,$_)[0,2,3,4] ;
	next if ($mg eq "WC-MedSvir") ;
	next if ($mg eq "WC-MedDvir") ;
	$MG{$mg} = 1 ;
	if ($type eq "Ribosomal") {
		$cel{$mg} += $rpkms ;
	} elsif ($type eq "MCPs") {
		$mcps{$mg} += $rpkms ;
		if ($class eq "Virus") {
			$vir{$mg} += $rpkms ;
		} elsif ($class eq "Prophage") {
			$phg{$mg} += $rpkms ;
		}
	} 
} close M ;
	
#	print "Metagenome\tMethod\tCells\tViralMCPs\tAllMCPs\n" ;
foreach my $mg (keys %MG) {
	if ($cel{$mg}) {
		print "$mg\tCells\t1\n"  ;
		if ($mcps{$mg}) {
			my $mcps  = $mcps{$mg}/$cel{$mg} ;
			print "$mg\tAllViruses\t$mcps\n"  ;
			if ($vir{$mg}) {
				my $viral = $vir{$mg}/$cel{$mg}  ;
				print "$mg\tVPs\t$viral\n"  ;
			#	my $mcps  = $mcps{$mg}/$cel{$mg} ;
			#	print "$mg\t1\t$viral\t$mcps\n"  ;
			} elsif ($phg{$mg}) {
				my $prophage = $phg{$mg}/$cel{$mg}  ;
				print "$mg\tProphages\t$prophage\n"  ;
			} else {
				print "$mg\tVPs\t0\n"  ;
			#	my $mcps  = $mcps{$mg}/$cel{$mg} ;
			#	print "$mg\t1\t0\t$mcps\n"  ;
			}
		} else {
			print "$mg\tAllViruses\t0\n"  ;
		#	print "$mg\t1\t0\t0\n"  ;
		}
	} else {
#		print "$mg\tCells\t0\n"  ;
		if ($mcps{$mg}) {
#			print "$mg\tAllMCPs\t$mcps{$mg}\n"  ;
			if ($vir{$mg}) {
#				print "$mg\tViralMCPs\t$vir{$mg}\n"  ;
			#	my $viral = $vir{$mg}  ;
			#	my $mcps  = $mcps{$mg} ;
			#	print "$mg\t0\t$viral\t$mcps\n"  ;
			} else {
#				print "$mg\tViralMCPs\t0\n"  ;
			#	my $mcps  = $mcps{$mg} ;
			#	print "$mg\t0\t0\t$mcps\n"  ;
			}
		} else {
#			print "$mg\tAllMCPs\t0\n"  ;
		#	print "$mg\t0\t0\t0\n"  ;
		}
	}
}

