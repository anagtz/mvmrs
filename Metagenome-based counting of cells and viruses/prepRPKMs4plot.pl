#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka/RPKMs" ;
my $code = $ARGV[0] ;

unless ($code) {
	print "Metagenome code (e.g. DAL, LLA9, WC, etc...): " ;
	chomp ($code = <STDIN>) ;
}

my $bae   = "Free Living (B + A + E)" ;
#my @mthds = qw(Prokka OrfM) ;
my (%MGs, %Classes) ;
#foreach my $code (@mthds) {

my $USCGs  = "$code"."_USCGsMarkers_PFAM_RPKM_indv.tab" ;
my $MCPs   = "$code"."_MCPs_RPKM_indv.tab" ;
	
#	my $rpkmsf = "$code"."_MCPs_RPKMperGene.tab" ;
#	open (RPKM, "$rpkmsf") || die "Can't open $rpkmsf\n" ;
	
	
my (%larger, $total, %wPfam, %previous) ;
print "Reading $USCGs\n" ;
open (RIB, "$USCGs") || die "Can't open $USCGs\n" ;
chomp (my $header = <RIB>) ;
my @metas  = (split/\t/,$header) ;
shift @metas ;
shift @metas ;
while (<RIB>) {
	chomp ;
	my @fields = (split/\t/,$_) ;
	my $pfam   = shift @fields  ;
	my $class  = shift @fields  ;
	$total     = $#fields       ;
	$Classes{$class} = 1        ;
	next if ($class eq "Virus") ;
		
	foreach my $i (0..$total) {
			
		$MGs{$metas[$i]} = 1 ;
		if ($metas[$i] =~ /PBS/) {
			$metas[$i] =~ s/PBS/LSSM/ ;
		}
			
		my $rpkm = $fields[$i] ;
#		print "$pfam\t$class\t$metas[$i]\t$rpkm\n" ; #<STDIN> ;
			
		if ($previous{$class}{$metas[$i]}) {
			if ($wPfam{$class}{$metas[$i]} ne $pfam) {
				if ($previous{$class}{$metas[$i]} >= $rpkm) {
				#	print "Previous RPKM value for $class and $metas[$i] is $previous{$class}{$metas[$i]} ($wPfam{$class}{$metas[$i]}) which is larger than actual $rpkm RPKMs ($pfam)\n" ;
				#	print "So I do nothing\n" ;
				} else {
				#	print "Previous RPKM value for $class and $metas[$i] is $previous{$class}{$metas[$i]} ($wPfam{$class}{$metas[$i]}) which is smaller than actual $rpkm RPKMs ($pfam)\n" ; ## <STDIN> ;
				#	print "So I keep new one: $pfam for $class in $metas[$i] with an RPKM value or $rpkm\n" ; <STDIN> ;
					$larger{$class}{$metas[$i]}   = $rpkm ;
					$previous{$class}{$metas[$i]} = $rpkm ;
					$wPfam{$class}{$metas[$i]}    = $pfam ;
				}
			} ## Is this useful?
		} else {
			$larger{$class}{$metas[$i]}   = $rpkm ;
			$previous{$class}{$metas[$i]} = $rpkm ;
			$wPfam{$class}{$metas[$i]}    = $pfam ;
		#	print "First time I see $class in $metas[$i], it's $pfam with $rpkm\n" ;
		}
	
	}
#	print "$gene\t$class\n" ;
} close RIB ;
	
my %MCPs ;
open (MCP, "$MCPs") || die "Can't open $MCPs\n" ;
chomp (my $header = <MCP>) ;
my @metas  = (split/\t/,$header) ;
shift @metas ;
shift @metas ;
while (<MCP>) {
	chomp ;
	my @fields = (split/\t/,$_) ;
	my $mcp    = shift @fields  ;
	my $class  = shift @fields  ;
	$Classes{$class} = 1        ;
	foreach my $i (0..$total) {
		$MGs{$metas[$i]} = 1    ;
		$MCPs{$class}{$metas[$i]} += $fields[$i] ;
	}
} close MCP ;

my $out = "$code"."_RPKMs_USCGsLarger_correctedMCPsSum.melted" ;
open (OUT, ">$out") || die "Can't create $out\n" ;
print OUT "Type\tGene\tMetagenome\tClass\tCount\n" ;
print     "Type\tGene\tMetagenome\tClass\tCount\n" ;

my $vir = "$code"."_RPKMs_USCGsLarger_viralMCPsSum.melted" ;
open (VIR, ">$vir") || die "Can't create $vir\n" ;
print VIR "Type\tGene\tMetagenome\tClass\tCount\n" ;

foreach my $meta (sort keys %MGs) {
	foreach my $class (sort keys %Classes) {
	#	print "$class\t$ribosum{$class}{$meta}*\t$total\n" ; #<STDIN> ;
	#	print "Ribosomal\t$meta\t$class\t$avg\n" ; <STDIN> ;
	#	if ($class =~ /Free/) {
	#		print BAE "Ribosomal\t$meta\t$class\t$avg\n" ;
	#	} else {
		
		
			if ($larger{$class}{$meta}) {
				print     "Ribosomal\t$wPfam{$class}{$meta}\t$meta\t$class\t$larger{$class}{$meta}\n" ; #<STDIN> ;
				print OUT "Ribosomal\t$wPfam{$class}{$meta}\t$meta\t$class\t$larger{$class}{$meta}\n" ;
				print VIR "Ribosomal\t$wPfam{$class}{$meta}\t$meta\t$class\t$larger{$class}{$meta}\n" ;
			} else {
			#	print     "Ribosomal\tNA\t$meta\t$class\t0\n" ; #<STDIN> ;
			#	print OUT "Ribosomal\tNA\t$meta\t$class\t0\n" ;
			#	print VIR "Ribosomal\tNA\t$meta\t$class\t0\n" ;
			}
			
			if ($MCPs{$class}{$meta}) {
				print     "MCPs\tSumMCPs\t$meta\t$class\t$MCPs{$class}{$meta}\n" ; #<STDIN> ;
				print OUT "MCPs\tSumMCPs\t$meta\t$class\t$MCPs{$class}{$meta}\n" ;
				if ($class eq "Virus") {
					print VIR "MCPs\tSumMCPs\t$meta\t$class\t$MCPs{$class}{$meta}\n" ;
				#	print OUT "MCPs\tSumMCPs\t$meta\t$class\t$MCPs{$class}{$meta}\n" ;
				} else {
				#	print OUT "MCPs\tSumMCPs\t$meta\tCellularMCP\t$MCPs{$class}{$meta}\n" ;
				## calculate 10% of Cellular MCPs (correction that compensates fpr the taxonomy assignment rate)
				#	next if ($meta eq "V001M00") ;
				#	next if ($meta eq "V000M01") ;
				#	my $tenperc = ($MCPs{$class}{$meta} * 10.4)/100 ;
				#	print OUT "MCPs\tSumMCPs\t$meta\tCellMCPcorr\t$tenperc\n" ;
				#	print "10.4% of $MCPs{$class}{$meta} is $tenperc RPKMs\n" ; <STDIN> ;
				}
			} else {
				print     "MCPs\tNA\t$meta\t$class\t0\n" ;# <STDIN> ;
			#	print OUT "MCPs\tNA\t$meta\tMCP_$class\t0\n" ;
				if ($class eq "Virus") {
				#	print VIR "MCPs\tNA\t$meta\t$class\t0\n" ;
				} else {
				#	print OUT "MCPs\tNA\t$meta\tCellularMCP\t0\n" ;
				}
			}
	#	}
	#	
	#	if ($class eq "DPANN" || $class eq "CPR" || $class eq "NotAssigned") {
	#		print BAE "Ribosomal\t$meta\t$class\t$avg\n" ;
	#	}
		
	}
}
	
	
#	### ADD PROPHAGES
#	my %rpkms ;
#	while (<RPKM>) {
#		chomp ;
#		my ($id, $mg, $class, $rpkms) = (split/\t/,$_)[0,1,5,6] ;
#		if ($phages{$id}) {
#		#	print "$id, $mg, $class, $rpkms\n" ;
#			$rpkms{$mg} += $rpkms ;
#		}
#	} close RPKM ;
#	
#	foreach my $mg (keys %rpkms) {
#		print OUT "MCPs\tSumMCPs\t$mg\tProphages\t$rpkms{$mg}\n" ;
#		print     "MCPs\tSumMCPs\t$mg\tProphages\t$rpkms{$mg}\n" ;
#	}
	
	
#}







