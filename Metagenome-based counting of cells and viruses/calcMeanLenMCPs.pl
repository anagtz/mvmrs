#!/usr/bin/perl -w

use strict ;

my $path = "/home/ana/LAB/Virus/NaturalMetagenomes/Annotation/Prokka" ;
my $desc = "$path"."/MCPs/MCPs.desc" ; 

open (DESC, "$desc") || die "Can't open $desc\n" ;
my $genes = "MCPs" ;

my %names ;

while (<DESC>) {
	chomp ;
	next if ($_ =~ /^\#/) ;
	my $mcp = (split/\t/,$_)[0] ;
	$names{$mcp} = 1 ;
}

#my @mthds = qw(Prokka OrfM) ;
#foreach my $mth (@mthds) {

foreach my $fna (<$path/MCPs/Seqs/*_viral_prophage_MCPs.ffn>) {
	my $fn = (split/\//,$fna)[-1]    ;
	my $mg = (split/\_viral/,$fn)[0] ;
	print "$mg...\n" ; #<STDIN> ;



	my (%tot, %len, %min, %max, %txpf) ;
#	my $suf = "$mth"."_MCPs_TaxaGroup.tab" ;
#	foreach my $txgp (<Taxa/V*M*_$suf>) {
#		my (%TX) ;
#		my $fn = (split/\//,$txgp)[-1] ;
#		my $mg = (split/\_/,$fn)[0]   ;
#		open (TX, "$txgp") || die "Can't open $txgp\n" ;
#		while (<TX>) {
#			chomp ;
#			my ($gene, $group) = (split/\t/,$_)[0,2]    ;
#			my ($id, $pfam)    = (split/\|/,$gene)[0,1] ;
#			next unless ($names{$pfam}) ;
#			$TX{$id} = $group ;
#		} close TX ;
#		my $fna = "$genes"."/"."$mg"."_"."$mth"."_"."$genes".".ffn" ;

	open (FNA, "$fna") || die "Can't open $fna\n" ;
	$/ = "\n\>" ;
#	<FNA> ;
	while (<FNA>) {
		chomp ;
		my @lines = (split/\n/,$_) ;
		my $head  = shift @lines ;
		$head =~ s/\>//g ;
			
		my ($id, $mcp, $taxa) = (split/\|/,$head)[0,1,2]  ;
		next unless ($names{$mcp}) ;
		
		my $seq ;
		foreach my $i (@lines) {
			$seq .= $i ;
		}
			
		my $l     = length($seq)    ;
			
		$len{$mcp} += $l ;
		$tot{$mcp} ++ ;	
			
		unless ($min{$mcp}) {
			$min{$mcp} = 1000000000000 ;
		}
		
		unless ($max{$mcp}) {
			$max{$mcp} = 0 ;
		}
		
		if ($l < $min{$mcp}) {
			$min{$mcp} = $l ;
	#		print "MIN $gene: $l\n" ; #<STDIN> ;
		}
			
		if ($l > $max{$mcp}) {
			$max{$mcp} = $l ;
	#		print "MAX $gene: $l\n" ; <STDIN> ;
		}
	}
	$/ = "\n" ;


	my $out = "$mg"."_meanlen_MCPs.tab" ;
	open (OUT, ">$out") || die "Can't create $out\n" ;

	print     "MCP\tMean Length\tMin Length\tMax Length\n" ;
	print OUT "MCP\tMean Length\tMin Length\tMax Length\n" ;



	foreach my $mcp (sort keys %names) {
		unless ($tot{$mcp}) {
			print "$mcp has no representatives in $mg Metagenome\n" ;
		} else {
			my $mean = ($len{$mcp})/($tot{$mcp}) ;
		#	my ($class, $pfam) = (split/\_/,$cpfam) ;
		#	my $name  = $names{$cmp} ;
		#	my $cname = "$class"."_"."$name" ;
			print OUT "$mcp\t$mean\t$min{$mcp}\t$max{$mcp}\n" ;
			print     "$mcp\t$mean\t$min{$mcp}\t$max{$mcp}\n" ; #<STDIN> ;
			#include names to sum different PFAMs into same gene
		}
	}
}
